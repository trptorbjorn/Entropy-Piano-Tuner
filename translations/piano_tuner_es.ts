<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="49"/>
        <source>About</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="58"/>
        <source>Entropy Piano Tuner</source>
        <translation>Entropy Piano Tuner</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="75"/>
        <source>Built on %1</source>
        <translation>Creado el %1</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="76"/>
        <source>by %1 and %2</source>
        <translation>por %1 y %2</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="78"/>
        <source>Based on</source>
        <translation>Creado con</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="89"/>
        <source>Copyright %1 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation>Copyright %1 Dept. of Theor. Phys. III, University of Würzburg. Todos los derechos reservados.</translation>
    </message>
    <message>
        <source>Copyright 2016 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="obsolete">Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. Todos los derechos reservados. {2016 ?}</translation>
    </message>
    <message>
        <source>Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="vanished">Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. Todos los derechos reservados.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="90"/>
        <source>This software is licensed under the terms of the %1. The source code can be accessed at %2.</source>
        <translation>Este software tiene licencia bajo los términos de %1. El código fuente puede obtenerse en %2.</translation>
    </message>
    <message>
        <source>This software is licensed unter the terms of the %1. The source code can be accessed at %2.</source>
        <translation type="vanished">Este software tiene licencia bajo los términos de %1. El código fuente puede obtenerse en %2.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="94"/>
        <source>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="96"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Damos las gracias a todos aquellos que han contribuido al proyecto:</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="120"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>AlgorithmDialog</name>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="83"/>
        <source>Algorithm:</source>
        <translation>Algortimo:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="201"/>
        <source>Info of algorithm: %1</source>
        <translation>Info del algortimo: %1</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="211"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="218"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="219"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="220"/>
        <source>Year:</source>
        <translation>Año:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="225"/>
        <source>Description:</source>
        <translation>Descripción:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="230"/>
        <source>Parameters</source>
        <translation>Parámetros</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="383"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="385"/>
        <source>Reset the parameter to its default value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalculationProgressGroup</name>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="69"/>
        <source>Status:</source>
        <translation>Estado:</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="116"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="179"/>
        <source>An unknown error occured during the calculation.</source>
        <translation>Un error desconocido ocurrió durante la operación.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="182"/>
        <source>No data available.</source>
        <translation>Datos no disponibles.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="185"/>
        <source>Not all keys recorded</source>
        <translation>Faltan teclas por grabar</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="188"/>
        <source>Key data inconsistent.</source>
        <translation>Datos de teclado inválidos.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="191"/>
        <source>Not enough keys recorded in left section.</source>
        <translation>No hay suficientes sonidos en el puente de graves.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="194"/>
        <source>Not enough keys recorded in right section.</source>
        <translation>Insuficiente de sonido grabados a lo largo del puente de agudos.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="197"/>
        <source>Undefined error message.</source>
        <translation>Mensaje de error sin definir.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="202"/>
        <source>Calculation error</source>
        <translation>Error de cálculo</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="203"/>
        <source>Error code</source>
        <translation>Código de error</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="155"/>
        <source>Calculation started</source>
        <translation>Operación comenzada</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="156"/>
        <source>Stop calculation</source>
        <translation>Operación terminada</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>Calculation finished</source>
        <translation>Cálculo terminado</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>The calculation finished successfully! Now you can switch to the tuning mode and tune your piano.</source>
        <translation>El cálculo terminó con éxito! Ahora se puede cambiar al modo de ajuste y afinar su piano.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="170"/>
        <source>Minimizing the entropy</source>
        <translation>Minizando la entropía</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="231"/>
        <source>Calculation with: %1</source>
        <translation>Cálculo usando: %1</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="264"/>
        <source>Calculation canceled</source>
        <translation>Operación cancelada</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="266"/>
        <location filename="../Qt/calculationprogressgroup.cpp" line="295"/>
        <source>Start calculation</source>
        <translation>Comenzar cálculo</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="291"/>
        <source>Press the button to start the calculation</source>
        <translation>Presione el botón para empezar la operación</translation>
    </message>
</context>
<context>
    <name>DoNotShowAgainMessageBox</name>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="29"/>
        <source>Do not show again.</source>
        <translation>No mostrar otra vez.</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="vanished">Reiniciar grabación</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">Restablecer marcadores de tono</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="34"/>
        <source>Clear pitch markers</source>
        <translation>Eliminar marcadores de tono</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="38"/>
        <source>Not all keys recorded</source>
        <translation>Faltan teclas por grabar</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="42"/>
        <source>Tuning curve not calculated</source>
        <translation>Curva de afinación no calculada</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="46"/>
        <source>Tuning curve must be recalculated</source>
        <translation>La curva de afinación debe ser recalculada</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="50"/>
        <source>Save changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="55"/>
        <source>Question</source>
        <translation>Pregunta</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="64"/>
        <source>Do you really want to clear all pitch markers? This can not be undone!</source>
        <translation>¿Realmente desea eliminar todos los marcadores de tono? ¡Esta operación no se puede deshacer!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="76"/>
        <source>Do you want to save your current changes? You can save at any time using the tool button or the action from the menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to reset all pitch markers? This can not be undone!</source>
        <translation type="vanished">¿Realmente desea restablecer todos los marcadores de tono? ¡Esta operación no se puede deshacer!</translation>
    </message>
    <message>
        <source>Do you really want to reset all recorded keys? This can not be made undone!</source>
        <translation type="vanished">¿Está seguro de querer reiniciar todos los datos grabados? ¡Esta operación no se puede deshacer!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="67"/>
        <source>Not all keys have been recorded. Switch the mode and record them.</source>
        <translation>Faltan teclas por grabar. Cambie el modo para grabarlas.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="70"/>
        <source>The tuning curve has not been calculated. Switch the mode and calculate it.</source>
        <translation>La curva de afinación no ha sido calculada. Cambie el modo para calcularla.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="73"/>
        <source>There are missing frequencies in the calculated tuning curve. Recalculate to fix this.</source>
        <translation>Faltan frecuencias en la curva de afinación calculada. Calcúlela de nuevo.</translation>
    </message>
</context>
<context>
    <name>EditPianoSheetDialog</name>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="14"/>
        <source>Piano data sheet</source>
        <translation>Datos del piano</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="35"/>
        <source>Piano operating site information</source>
        <translation>Localización del piano</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="44"/>
        <source>Tuning location</source>
        <translation>Afinado en</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="54"/>
        <source>Time of tuning</source>
        <translation>Última afinación</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="82"/>
        <source>Now</source>
        <translation>Hoy</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="91"/>
        <source>Concert pitch</source>
        <translation>Tono de concierto</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="106"/>
        <source>A</source>
        <translation>LA</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="128"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="230"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="264"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="322"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="140"/>
        <source>Piano manufacturer information</source>
        <translation>Información del fabricante</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="149"/>
        <source>Piano name</source>
        <translation>Nombre del piano</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="159"/>
        <source>Serial number</source>
        <translation>Número de serie</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="169"/>
        <source>Manufaction year</source>
        <translation>Año de construcción</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="195"/>
        <source>Production location</source>
        <translation>Lugar de construcción</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="205"/>
        <source>Number of keys</source>
        <translation>Número de teclas</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="239"/>
        <source>Key number of A</source>
        <translation>Número de tecla LA</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="273"/>
        <source>Piano type</source>
        <translation>Tipo de piano</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="287"/>
        <source>Grand</source>
        <translation>de Cola</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="292"/>
        <source>Upright</source>
        <translation>de Pared</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="300"/>
        <source>Keys on bass bridge</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FullScreenKeyboardDialog</name>
    <message>
        <location filename="../Qt/keyboard/fullscreenkeyboarddialog.cpp" line="30"/>
        <source>Keyboard</source>
        <translation>Teclado</translation>
    </message>
</context>
<context>
    <name>InitializeDialog</name>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="69"/>
        <source>Initializing the core component</source>
        <translation>Inicializando el componente principal</translation>
    </message>
    <message>
        <source>Preparing</source>
        <translation type="vanished">Preparando</translation>
    </message>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="98"/>
        <source>Initializing, please wait</source>
        <translation>Inicializando, por favor espere</translation>
    </message>
    <message>
        <source>Initializing the midi component</source>
        <translation type="vanished">Inicializando el componente midi</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">Finalizado</translation>
    </message>
</context>
<context>
    <name>KeyboardGraphicsView</name>
    <message>
        <location filename="../Qt/keyboard/keyboardgraphicsview.cpp" line="78"/>
        <source>This window displays the keyboard. Each key has a small indicator that will display the current recording state of this key.</source>
        <translation>Esta ventana muestra el teclado. Cada tecla tiene un pequeño indicador que mostrará el estado actual de la grabación de esta clave.</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../Qt/logviewer.ui" line="20"/>
        <location filename="../Qt/logviewer.ui" line="36"/>
        <source>Log</source>
        <translation>Registro (Log)</translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="56"/>
        <source>Copy the contents of the log to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="59"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Qt/mainwindow.ui" line="14"/>
        <location filename="../Qt/mainwindow.cpp" line="545"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="188"/>
        <source>Entropy piano tuner</source>
        <translation>Entropy piano tuner</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This windows displays the tuning curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esta ventana muestra la curva de afinación.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This window displays the recorded spectrum of a single note. Bars will indicate the peaks that were found during the analysis.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esta ventana muestra el espectro de una sola nota. Las barras indican los picos encontrados durante el análisis.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="76"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="89"/>
        <source>&amp;Tools</source>
        <translation>&amp;Herramientas</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="98"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="128"/>
        <source>&amp;Open</source>
        <translation>&amp;Abrir</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="133"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="138"/>
        <source>E&amp;xit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="143"/>
        <location filename="../Qt/mainwindow.cpp" line="210"/>
        <source>About</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="148"/>
        <source>Open sound control</source>
        <translation>Abrir control de sonido</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="153"/>
        <source>Save &amp;As</source>
        <translation>Guardar &amp;como</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="158"/>
        <source>&amp;New</source>
        <translation>&amp;Nuevo</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="163"/>
        <source>&amp;Edit piano data sheet</source>
        <translation>&amp;Editar datos del piano</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="173"/>
        <source>Share</source>
        <translation>Compartir</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="181"/>
        <source>View log</source>
        <translation>Ver archivo de registro</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="186"/>
        <source>&amp;Options</source>
        <translation>&amp;Option</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="191"/>
        <source>&amp;Clear pitch markers</source>
        <translation>&amp;Eliminar marcadores de tono.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="194"/>
        <location filename="../Qt/mainwindow.cpp" line="190"/>
        <source>Clear pitch markers</source>
        <translation>Eliminar marcadores de tono.</translation>
    </message>
    <message>
        <source>&amp;Reset pitch markers</source>
        <translation type="vanished">&amp;Restablecer marcadores de tono</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">Restablecer marcadores de tono</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="obsolete">Reiniciar grabación</translation>
    </message>
    <message>
        <source>&amp;Reset recoding</source>
        <translation type="vanished">&amp;Reiniciar grabación</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="199"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="204"/>
        <location filename="../Qt/mainwindow.cpp" line="208"/>
        <source>Tutorial</source>
        <translation>Tutorial</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="143"/>
        <source>Idle</source>
        <translation>Inactive</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="149"/>
        <source>Record</source>
        <translation>Grabar</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="155"/>
        <source>Calculate</source>
        <translation>Calcular</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="161"/>
        <source>Tune</source>
        <translation>Afinar</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="145"/>
        <source>Press this button to switch to the idle mode.</source>
        <translation>Presionar el botón para cambiar a modo inactivo.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="151"/>
        <source>Press this button to switch to the recording mode.</source>
        <translation>Presionar el botón para cambiar a modo de grabación.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="157"/>
        <source>Press this button to switch to the calculation mode.</source>
        <translation>Presionar el botón para cambiar a modo de operación.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="163"/>
        <source>Press this button to switch to the tuning mode.</source>
        <translation>Presionar el botón para cambiar a modo de afinación.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="179"/>
        <source>Documents and tools</source>
        <translation>Documentos y herramientas</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="185"/>
        <source>New</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="188"/>
        <source>Save as</source>
        <translation>Guardar como</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="189"/>
        <source>Edit piano data sheet</source>
        <translation>Editar datos del piano</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="192"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="193"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="195"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <source>Log</source>
        <translation type="vanished">Registro (Log)</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="230"/>
        <source>Export</source>
        <translation>Exportar datos</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="371"/>
        <source>File created</source>
        <translation>Archivo creado</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="374"/>
        <source>File edited</source>
        <translation>Archivo editado</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="377"/>
        <source>File opened</source>
        <translation>Archivo abierto</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="380"/>
        <source>File saved</source>
        <translation>Archivo guardado</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="458"/>
        <source>Recording keystroke</source>
        <translation>Grabando tecla</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="466"/>
        <source>Signal analysis started</source>
        <translation>Análisis de señal comenzado</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="469"/>
        <source>Signal analysis ended</source>
        <translation>Análisis de señal finalizado</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="498"/>
        <source>Calculation failed</source>
        <translation>La operación falló</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="501"/>
        <source>Calculation ended</source>
        <translation>Operación finalizada</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="504"/>
        <source>Calculation started</source>
        <translation>Operación comenzada</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <source>Unable to find a supported sound control.</source>
        <translation>Control de sistema no encontrado.</translation>
    </message>
    <message>
        <source>Unable to find a supported sound conrol.</source>
        <translation type="vanished">Control de sistema no encontrado.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>Canceled</source>
        <translation>Cancelado</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>You need to save the file before you can share it.</source>
        <translation>Debe guardar el archivo antes de compartirlo.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="788"/>
        <source>A new update is available!</source>
        <translation>Una nueva actualización está disponible!</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="789"/>
        <source>The online app version is %1. Do you want to install this update?</source>
        <translation>La versión de la aplicación en línea es %1. ¿Quieres instalar esta actualización?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>The maintenance tool could not be started automatically. To update the program you have to start the maintenance tool manually.</source>
        <translation>La herramienta de mantenimiento no pudo iniciarse automáticamente. Para actualizar el programa que tiene que comenzar la herramienta de mantenimiento manualmente.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="71"/>
        <source>The document has been modified.</source>
        <translation>El documento ha sido modificado.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="72"/>
        <source>Do you want to save your changes?</source>
        <translation>¿Desea guardar los cambios?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="187"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="95"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="186"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="119"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="137"/>
        <source>Share tuning data</source>
        <translation>Compartir datos de afinación</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="154"/>
        <source>New piano</source>
        <translation>Nuevo piano</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="157"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="159"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <source>File could not be opened.</source>
        <translation>No se pudo abrir el archivo.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>File could not be saved.</source>
        <translation>No se pudo guardar el archivo.</translation>
    </message>
    <message>
        <source>Entopy piano tuner</source>
        <translation type="obsolete">Entropy piano tuner</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="191"/>
        <source>Comma-separated values</source>
        <translation>Valores Separados por Comas</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="194"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
</context>
<context>
    <name>PlotsDialog</name>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="106"/>
        <source>Reset view</source>
        <translation>Restablecer vista</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="107"/>
        <source>Go first</source>
        <translation>Ir al primer</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="108"/>
        <source>Go previous</source>
        <translation>Ir a la anterior</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="109"/>
        <source>Go next</source>
        <translation>Ir al siguiente</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="110"/>
        <source>Go last</source>
        <translation>Ir a la última</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="116"/>
        <source>Inh.</source>
        <translation>Inh.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="117"/>
        <source>Rec.</source>
        <translation>Rec.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="118"/>
        <source>Comp.</source>
        <translation>Cmp.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="119"/>
        <source>Tun.</source>
        <translation>Tun.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="124"/>
        <source>Tuned</source>
        <translation>Afinado</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="121"/>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="283"/>
        <source>Inharmonicity</source>
        <translation>Inarmonía</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="122"/>
        <source>Recorded</source>
        <translation>Grabado</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="123"/>
        <source>Computed</source>
        <translation>Computada</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="137"/>
        <source>Key index</source>
        <translation>Índice de la clave</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="287"/>
        <source>Frequency deviation [cent]</source>
        <translation>Desviación de frecuencia [cent]</translation>
    </message>
</context>
<context>
    <name>ProgressDisplay</name>
    <message>
        <location filename="../Qt/progressdisplay.cpp" line="56"/>
        <source>Synthesizer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="982"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="985"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="988"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="993"/>
        <source>Images</source>
        <translation>Imágenes</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="1008"/>
        <source>Export File Name</source>
        <translation>Nombre del archivo de exportación</translation>
    </message>
</context>
<context>
    <name>RecordingQualityBar</name>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="40"/>
        <source>Quality</source>
        <translation>Calidad</translation>
    </message>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="41"/>
        <source>This bar displays the quality of the recording. All of the recorded keys should have an almost equal quality before starting the calculation.</source>
        <translation>Esta barra muestra la calidad de la grabación. Todas las teclas grabada debe tener una calidad casi igual antes de iniciar el cálculo.</translation>
    </message>
</context>
<context>
    <name>RecordingStatusGraphicsView</name>
    <message>
        <location filename="../Qt/recordingstatusgraphicsview.cpp" line="33"/>
        <source>This item displays the status of the recorder. A red circle indicates that the audio signal is currently recorded. A blue rotating circle is shown when the program processes the recorded signal. A green pause symbol is displayed if you can record the next key.</source>
        <translation>Este elemento muestra el estado de la grabadora. Un círculo rojo indica que la señal de audio se graba actualmente. Un círculo que gira azul se muestra cuando el programa procesa la señal grabada. Se muestra un símbolo de pausa verde si puede grabar la siguiente tecla.</translation>
    </message>
</context>
<context>
    <name>SignalAnalyzerGroupBox</name>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="31"/>
        <source>Signal analyzer</source>
        <translation>Analizador de señal</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="68"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="78"/>
        <source>This label displays the current selected key.</source>
        <translation>Esta etiqueta muestra la clave seleccionada actual.</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="79"/>
        <source>This label shows the ground frequency of the selected key.</source>
        <translation>Esta etiqueta muestra la frecuencia fundamental de la tecla seleccionada.</translation>
    </message>
</context>
<context>
    <name>SimpleFileDialog</name>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="92"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="148"/>
        <source>File existing</source>
        <translation>Archivo existente</translation>
    </message>
    <message>
        <source>A file with the given name already exits at %1. Do you want to overwrite it?</source>
        <translation type="vanished">Un archivo con el nombre dado ya existe en %1. ¿Quieres sobreescribirlo?</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="149"/>
        <source>A file with the given name already exists at %1. Do you want to overwrite it?</source>
        <translation>Un archivo con el nombre dado ya existe en %1. ¿Quieres sobreescribirlo?</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Invalid filename</source>
        <translation>Nombre de archivo inválido</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Please provide a valid filename.</source>
        <translation>Por favor ingrese su nombre de archivo válido.</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="178"/>
        <source>Remove file</source>
        <translation>Eliminar archivo</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="179"/>
        <source>Are you sure that you really want to delete the file &quot;%1&quot;?</source>
        <translation>¿Seguro de que realmente desea eliminar el archivo &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>TunerApplication</name>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>Crash handler</source>
        <translation>Manejador de Crash</translation>
    </message>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>The application exited unexpectedly on the last run. Do you want to view the last log?</source>
        <translation>La aplicación se estrelló inesperadamente en la última ejecución. ¿Quieres ver el último registro?</translation>
    </message>
</context>
<context>
    <name>TuningGroupBox</name>
    <message>
        <source>Tuning</source>
        <translation type="vanished">Afinación</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorGroupBox</name>
    <message>
        <location filename="../Qt/tuningindicatorgroupbox.cpp" line="27"/>
        <source>Tuning</source>
        <translation type="unfinished">Afinación</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorView</name>
    <message>
        <location filename="../Qt/tuningindicatorview.cpp" line="46"/>
        <source>This is the tuning indicator. Touch this field to toggle between spectral and stroboscopic mode. In the spectral mode you should bring the peak to the center of the window for optimal tuning. When tuning several strings of unisons at once, several peaks might appear and all of them should be tuned to match the center. In the stroboscopic mode several stripes of drifiting rainbows are shown. The stripes represent the partials. Optimal tuning of single strings is obtained when the rainbows come to a halt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeControlGroupBox</name>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="37"/>
        <source>Volume control</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="46"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="131"/>
        <source>Click this button to mute the speaker or headphone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="142"/>
        <source>This widgets provides settings and information about the input level of the input device.</source>
        <translation>Este widget muestra los ajustes e información del volumen de entrada del dispositivo de entrada.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="143"/>
        <source>If the input level drops below this mark the recorder stops and does not process the input signal.</source>
        <translation>Si el volumen baja de esta marca, la grabación se interrumpe y la señal de entrada no se procesa.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="144"/>
        <source>If the input level reaches this threshold the recorder starts analyzing the signal of the input device until the level drops below the &apos;Off&apos; mark.</source>
        <translation>Si el volumen alcanza esta marca, comienza la grabación y se analiza la señal del dispositivo de entrada hasta que el volumen cae por debajo de la marca &apos;Off&apos;.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="145"/>
        <source>Click this button to reset the automatic calibration of the input volume.</source>
        <translation>Haga clic en este botón para restablecer la calibración automática del volumen de entrada.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="146"/>
        <source>Click this button to mute the input device.</source>
        <translation>Haga clic en este botón para silenciar el dispositivo de entrada.</translation>
    </message>
</context>
<context>
    <name>VolumeControlLevel</name>
    <message>
        <location filename="../Qt/volumecontrollevel.cpp" line="32"/>
        <source>This bar displays the current input level.</source>
        <translation>Esta barra muestra el.</translation>
    </message>
</context>
<context>
    <name>ZoomedSpectrumGraphicsView</name>
    <message>
        <source>This is the tuning device. You should bring the peak and the indicator bar in the middle of the window for an optimal tuning. When tuning several strings at once, there might appear several peaks. All of them should be tuned to match the center.</source>
        <translation type="vanished">Este es el dispositivo de sintonización. Usted debe llevar el pico y la barra indicadora en el centro de la ventana para una sintonización óptima. Al sintonizar varias cuerdas a la vez, puede aparecer varias peaks. Todos ellos deben ser afinado para que coincida con el centro.</translation>
    </message>
</context>
<context>
    <name>options::OptionsDialog</name>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="48"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="70"/>
        <source>Environment</source>
        <translation>Entorno</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="71"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
</context>
<context>
    <name>options::PageAudio</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="31"/>
        <source>Input device</source>
        <translation>Dispositivo de entrada</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="32"/>
        <source>Output device</source>
        <translation>Dispositivo de salida</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="33"/>
        <source>Midi</source>
        <translation>Midi</translation>
    </message>
</context>
<context>
    <name>options::PageAudioInputOutput</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="64"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="70"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="95"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="111"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <source>Input device</source>
        <translation type="vanished">Dispositivo de entrada</translation>
    </message>
    <message>
        <source>Sample rates</source>
        <translation type="vanished">Tasa de muestreo</translation>
    </message>
    <message>
        <source>Sampling rates</source>
        <translation type="vanished">Índices de muestreo</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="65"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="71"/>
        <source>Sampling rate </source>
        <translation>Tasa de muestreo</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="77"/>
        <source>Open system settings</source>
        <translation>Abrir opciones de sistema</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="90"/>
        <source>Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="102"/>
        <source>Buffer size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>You need at least a sampling rate of %1 to record and play all keys.</source>
        <translation>Necesita una tasa de muestreo de al menos %1 para poder grabar y tocar todas las notas.</translation>
    </message>
</context>
<context>
    <name>options::PageAudioMidi</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudiomidipage.cpp" line="33"/>
        <source>Midi device</source>
        <translation>Dispositivo midi</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironment</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="29"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="30"/>
        <source>Tuning</source>
        <translation>Afinación</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentGeneral</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="47"/>
        <source>User Interface</source>
        <translation>Interfaz de usuario</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="54"/>
        <source>Language</source>
        <translation>Lenguaje</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="56"/>
        <source>&lt;System Language&gt;</source>
        <translation>&lt;Idioma del sistema&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="83"/>
        <source>Reactivate warnings</source>
        <translation>Reactivar advertencias</translation>
    </message>
    <message>
        <source>Show all warnings</source>
        <translation type="vanished">Mostrar todas las advertencias</translation>
    </message>
    <message>
        <source>Reset warnings</source>
        <translation type="vanished">Limpiar advertencias</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="107"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="108"/>
        <source>The language change will take effect after a restart of the entropy piano tuner.</source>
        <translation>El cambio de lenguaje se hará efectivo después de reiniciar el programa.</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentTuning</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="35"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="36"/>
        <source>Synthesized key sound</source>
        <translation>Sonido de la tecla reconstruido</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="37"/>
        <source>Reference key</source>
        <translation>Tecla de referencia</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="38"/>
        <source>Synthesizer mode</source>
        <translation>Modo sintetizador</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="42"/>
        <source>Dynamic synthesizer volume</source>
        <translation>Volumen dinámico de sintetizador</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="48"/>
        <source>Stroboscopic tuning indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="54"/>
        <source>Disable automatic key selection</source>
        <translation>Desactivar la selección automática de claves</translation>
    </message>
</context>
</TS>
