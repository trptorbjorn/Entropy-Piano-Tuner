# Entropy Piano Tuner

## Information
For general information about the software have a look at the [homepage](http://piano-tuner.org/) of the project.

## Building
Regarding development please have a look at the [developer pages](http://develop.piano-tuner.org) for further information.