<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="49"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="58"/>
        <source>Entropy Piano Tuner</source>
        <translation>Энтропийный Тюнер для Фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="75"/>
        <source>Built on %1</source>
        <translation>Созданно %1</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="76"/>
        <source>by %1 and %2</source>
        <translation>%1 и %2</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="78"/>
        <source>Based on</source>
        <translation>Основан на</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="89"/>
        <source>Copyright %1 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation>Все права на принадлежат Кафедре теоретической физики III университета Вюрцбург, %1г.</translation>
    </message>
    <message>
        <source>Copyright 2016 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="obsolete">Все права на принадлежат Кафедре теоретической физики III университета Вюрцбург, 2015г. {2016 ?}</translation>
    </message>
    <message>
        <source>Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="vanished">Все права на принадлежат Кафедре теоретической физики III университета Вюрцбург, 2015г.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="90"/>
        <source>This software is licensed under the terms of the %1. The source code can be accessed at %2.</source>
        <translation>Это программное обеспечение лицензируется на условиях %1. Исходный код можно получить на %2.</translation>
    </message>
    <message>
        <source>This software is licensed unter the terms of the %1. The source code can be accessed at %2.</source>
        <translatorcomment>Bin mir nicht sicher</translatorcomment>
        <translation type="vanished">Это программное обеспечение лицензируется на условиях %1. Исходный код можно получить на %2.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="94"/>
        <source>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>Программа предоставляется с надеждой на то, что она пригодится пользователям, но она предоставляется БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, так же не гарантируется её ПРИГОДНОСТИ ДЛЯ КАКИХ-ЛИБО КОНКРЕТНЫХ ЦЕЛЕЙ.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="96"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Мы благодарим всех тех, кто внес свой вклад в этот проект:</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="120"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>AlgorithmDialog</name>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="83"/>
        <source>Algorithm:</source>
        <translation>Алгоритм:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="201"/>
        <source>Info of algorithm: %1</source>
        <translation>Информация алгоритма: %1</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="211"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="218"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="219"/>
        <source>Author:</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="220"/>
        <source>Year:</source>
        <translation>Год:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="225"/>
        <source>Description:</source>
        <translation>Описание:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="230"/>
        <source>Parameters</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="383"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="385"/>
        <source>Reset the parameter to its default value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalculationProgressGroup</name>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="69"/>
        <source>Status:</source>
        <translation>Статус:</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="116"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="179"/>
        <source>An unknown error occured during the calculation.</source>
        <translation>В процессе вычисления произошла неизвестная ошибка.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="182"/>
        <source>No data available.</source>
        <translation>Данные недоступны.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="185"/>
        <source>Not all keys recorded</source>
        <translation>Не все клавиши записаны</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="188"/>
        <source>Key data inconsistent.</source>
        <translation>Данные противоречивы.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="191"/>
        <source>Not enough keys recorded in left section.</source>
        <translation>Слишком мало ключи, записанные в басу.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="194"/>
        <source>Not enough keys recorded in right section.</source>
        <translation>Слишком мало ключи, записанные в высоких частот.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="197"/>
        <source>Undefined error message.</source>
        <translation>Неопределенная сообщение об ошибке.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="202"/>
        <source>Calculation error</source>
        <translation>Ошибка в вычислениях</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="203"/>
        <source>Error code</source>
        <translation>Код ошибки</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="155"/>
        <source>Calculation started</source>
        <translation>Вычисления начаты</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="156"/>
        <source>Stop calculation</source>
        <translation>Остоновить вычисления</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>Calculation finished</source>
        <translation>Расчет закончен</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>The calculation finished successfully! Now you can switch to the tuning mode and tune your piano.</source>
        <translation>Расчет успешно завершен!  Теперь вы можете перейти в режим настройки и настроить Ваше фортепиано. </translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="170"/>
        <source>Minimizing the entropy</source>
        <translation>Поиск минимума энтропии</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="231"/>
        <source>Calculation with: %1</source>
        <translation>Расчет с: %1</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="264"/>
        <source>Calculation canceled</source>
        <translation>Расчёт отменён</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="266"/>
        <location filename="../Qt/calculationprogressgroup.cpp" line="295"/>
        <source>Start calculation</source>
        <translation>Начать вычисления</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="291"/>
        <source>Press the button to start the calculation</source>
        <translation>Нажмите кнопку для начала вычислений</translation>
    </message>
</context>
<context>
    <name>DoNotShowAgainMessageBox</name>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="29"/>
        <source>Do not show again.</source>
        <translation>Больше не показывать.</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="vanished">Сброс записи</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">Сброс основного тона маркеры</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="34"/>
        <source>Clear pitch markers</source>
        <translation>Сброс основного тона маркеры</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="38"/>
        <source>Not all keys recorded</source>
        <translation>Не все клавиши записаны</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="42"/>
        <source>Tuning curve not calculated</source>
        <translation>Кривая настройки не рассчитана</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="46"/>
        <source>Tuning curve must be recalculated</source>
        <translation>Кривая настройки должна быть пересчитана</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="50"/>
        <source>Save changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="55"/>
        <source>Question</source>
        <translation>Вопрос</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="64"/>
        <source>Do you really want to clear all pitch markers? This can not be undone!</source>
        <translation>Вы действительно хотите сбросить все основного тона маркеры? Это не может быть отменено!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="76"/>
        <source>Do you want to save your current changes? You can save at any time using the tool button or the action from the menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to reset all pitch markers? This can not be undone!</source>
        <translation type="vanished">Вы действительно хотите сбросить все основного тона маркеры? Это не может быть отменено!</translation>
    </message>
    <message>
        <source>Do you really want to reset all recorded keys? This can not be made undone!</source>
        <translation type="vanished">Вы действительно хотите сбросить все записанные клавиши? Информация будет утеряна безвозвратно!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="67"/>
        <source>Not all keys have been recorded. Switch the mode and record them.</source>
        <translation>Не все клавиши были записаны. Измените режим и запишите их.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="70"/>
        <source>The tuning curve has not been calculated. Switch the mode and calculate it.</source>
        <translation> Рассчитаная кривая не была  вычислена. Переключите режим и вычислите её.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="73"/>
        <source>There are missing frequencies in the calculated tuning curve. Recalculate to fix this.</source>
        <translation>Нехватает некоторых чистот в кривой настройки. Пересчитайте для устранения ошибки.</translation>
    </message>
</context>
<context>
    <name>EditPianoSheetDialog</name>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="14"/>
        <source>Piano data sheet</source>
        <translation>Паспорт фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="35"/>
        <source>Piano operating site information</source>
        <translation>Информация о настройке фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="44"/>
        <source>Tuning location</source>
        <translation>Место настройки</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="54"/>
        <source>Time of tuning</source>
        <translation>Время настройки</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="82"/>
        <source>Now</source>
        <translation>Сейчас</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="91"/>
        <source>Concert pitch</source>
        <translation>Камертон</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="106"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="128"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="230"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="264"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="322"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="140"/>
        <source>Piano manufacturer information</source>
        <translation>Информация о производителе инструмента</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="149"/>
        <source>Piano name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="159"/>
        <source>Serial number</source>
        <translation>Серийный номер</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="169"/>
        <source>Manufaction year</source>
        <translation>Год выпуска</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="195"/>
        <source>Production location</source>
        <translation>Место производства</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="205"/>
        <source>Number of keys</source>
        <translation>Количество клавиш</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="239"/>
        <source>Key number of A</source>
        <translation>Количество клавиш А</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="273"/>
        <source>Piano type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="287"/>
        <source>Grand</source>
        <translation>Рояль</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="292"/>
        <source>Upright</source>
        <translation>Пианино</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="300"/>
        <source>Keys on bass bridge</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FullScreenKeyboardDialog</name>
    <message>
        <location filename="../Qt/keyboard/fullscreenkeyboarddialog.cpp" line="30"/>
        <source>Keyboard</source>
        <translation>Клавиатура</translation>
    </message>
</context>
<context>
    <name>InitializeDialog</name>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="69"/>
        <source>Initializing the core component</source>
        <translation>Инициализация основных компонентов</translation>
    </message>
    <message>
        <source>Preparing</source>
        <translation type="vanished">Подготовка</translation>
    </message>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="98"/>
        <source>Initializing, please wait</source>
        <translation>Инициализация, пожалуйста подождите</translation>
    </message>
    <message>
        <source>Initializing the midi component</source>
        <translation type="vanished">Инициализация MIDI компонентов</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">Завершено</translation>
    </message>
</context>
<context>
    <name>KeyboardGraphicsView</name>
    <message>
        <location filename="../Qt/keyboard/keyboardgraphicsview.cpp" line="78"/>
        <source>This window displays the keyboard. Each key has a small indicator that will display the current recording state of this key.</source>
        <translation>В этом окне отображается клавиатура. Каждая клавиша имеет небольшой индикатор, который будет отображать текущее состояние записи этой клавиши.</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../Qt/logviewer.ui" line="20"/>
        <location filename="../Qt/logviewer.ui" line="36"/>
        <source>Log</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="56"/>
        <source>Copy the contents of the log to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="59"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Qt/mainwindow.ui" line="14"/>
        <location filename="../Qt/mainwindow.cpp" line="545"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="188"/>
        <source>Entropy piano tuner</source>
        <translation>Энтропийный Тюнер для Фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This windows displays the tuning curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Это окно показывает кривую настройки.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This window displays the recorded spectrum of a single note. Bars will indicate the peaks that were found during the analysis.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Это окно показывает записанный спектр единичных тонов. Балки показывают пики, которые были найдины во время анализа.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="76"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="89"/>
        <source>&amp;Tools</source>
        <translation>&amp;Инструменты</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="98"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="128"/>
        <source>&amp;Open</source>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="133"/>
        <source>&amp;Save</source>
        <translation>&amp;_Сохранить</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="138"/>
        <source>E&amp;xit</source>
        <translation>Вы&amp;ход</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="143"/>
        <location filename="../Qt/mainwindow.cpp" line="210"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="148"/>
        <source>Open sound control</source>
        <translation>Открыть управление звуком</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="153"/>
        <source>Save &amp;As</source>
        <translation>Сохранить &amp;как</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="158"/>
        <source>&amp;New</source>
        <translation>&amp;Новый</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="163"/>
        <source>&amp;Edit piano data sheet</source>
        <translation>Изменить &amp;паспорт фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="173"/>
        <source>Share</source>
        <translation>Поделиться</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="181"/>
        <source>View log</source>
        <translation>Посмотреть журнал</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="186"/>
        <source>&amp;Options</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="191"/>
        <source>&amp;Clear pitch markers</source>
        <translation>&amp;Сброс основного тона маркеры</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="194"/>
        <location filename="../Qt/mainwindow.cpp" line="190"/>
        <source>Clear pitch markers</source>
        <translation>Сброс основного тона маркеры</translation>
    </message>
    <message>
        <source>&amp;Reset pitch markers</source>
        <translation type="vanished">&amp;Сброс основного тона маркеры</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">Сброс основного тона маркеры</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="obsolete">Сброс записи</translation>
    </message>
    <message>
        <source>&amp;Reset recoding</source>
        <translation type="vanished">Сбросить &amp;запись</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="199"/>
        <source>Manual</source>
        <translation>Руководство</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="204"/>
        <location filename="../Qt/mainwindow.cpp" line="208"/>
        <source>Tutorial</source>
        <translation>Обучение</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="143"/>
        <source>Idle</source>
        <translation>Ожидание</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="145"/>
        <source>Press this button to switch to the idle mode.</source>
        <translation>Нажмите эту кнопку для переключения в режим ожидания.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="149"/>
        <source>Record</source>
        <translation>Запись</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="151"/>
        <source>Press this button to switch to the recording mode.</source>
        <translation>Нажмите эту кнопку, чтобы перейти в режим записи.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="155"/>
        <source>Calculate</source>
        <translation>Вычислить</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="157"/>
        <source>Press this button to switch to the calculation mode.</source>
        <translation>Нажмите эту кнопку, чтобы переключиться в режим расчета.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="161"/>
        <source>Tune</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="163"/>
        <source>Press this button to switch to the tuning mode.</source>
        <translation>Нажмите эту кнопку, чтобы перейти в режим настройки.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="179"/>
        <source>Documents and tools</source>
        <translation>Документы и инструменты</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="185"/>
        <source>New</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="188"/>
        <source>Save as</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="189"/>
        <source>Edit piano data sheet</source>
        <translation>Изменить паспорт фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="192"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="193"/>
        <source>Graphs</source>
        <translation>Диаграммы</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="195"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <source>Log</source>
        <translation type="vanished">Журнал</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="230"/>
        <source>Export</source>
        <translation>Экспортировать</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="371"/>
        <source>File created</source>
        <translation>Файл создан</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="374"/>
        <source>File edited</source>
        <translation>Файл изменён</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="377"/>
        <source>File opened</source>
        <translation>Файл открыт</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="380"/>
        <source>File saved</source>
        <translation>Файл сохранён</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="458"/>
        <source>Recording keystroke</source>
        <translation>Запись клавиш</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="466"/>
        <source>Signal analysis started</source>
        <translation>Анализ сигнала начат</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="469"/>
        <source>Signal analysis ended</source>
        <translation>Анализ сигнала закончен</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="498"/>
        <source>Calculation failed</source>
        <translation>Вычисления не удались</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="501"/>
        <source>Calculation ended</source>
        <translation>Вычисления  закончены</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="504"/>
        <source>Calculation started</source>
        <translation>Вычисления  закончены</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <source>Unable to find a supported sound control.</source>
        <translation>Невозможно найти поддерживаемую регулировку звука.</translation>
    </message>
    <message>
        <source>Unable to find a supported sound conrol.</source>
        <translation type="vanished">Невозможно найти поддерживаемую регулировку звука.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>Canceled</source>
        <translation>Отменено</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>You need to save the file before you can share it.</source>
        <translation>Вам необходимо сохранить файл перед тем, как Вы сможете им поделиться.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="788"/>
        <source>A new update is available!</source>
        <translation>Обновление доступно!</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="789"/>
        <source>The online app version is %1. Do you want to install this update?</source>
        <translation>Онлай версия приложения %1.Вы хотите, чтобы установить это обновление?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>The maintenance tool could not be started automatically. To update the program you have to start the maintenance tool manually.</source>
        <translation>Инструмент обслуживания не может быть запущен автоматически. Чтобы обновить программу, вы должны начать инструмент обслуживания вручную.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="71"/>
        <source>The document has been modified.</source>
        <translation>Документ был изменён.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="72"/>
        <source>Do you want to save your changes?</source>
        <translation>Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="187"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="95"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="186"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="119"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="137"/>
        <source>Share tuning data</source>
        <translation>Поделиться тюнинговыми данными</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="154"/>
        <source>New piano</source>
        <translation>Новое фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="157"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="159"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <source>File could not be opened.</source>
        <translation>Не удаётся открыть файл.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>File could not be saved.</source>
        <translation>Не удаётся сохранить файл.</translation>
    </message>
    <message>
        <source>Entopy piano tuner</source>
        <translation type="obsolete">Энтропийный Тюнер для Фортепиано</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="191"/>
        <source>Comma-separated values</source>
        <translation>Значения разделенные запятыми</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="194"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
</context>
<context>
    <name>PlotsDialog</name>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="106"/>
        <source>Reset view</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="107"/>
        <source>Go first</source>
        <translation>Перейти к первой</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="108"/>
        <source>Go previous</source>
        <translation>Перейти к предыдущей</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="109"/>
        <source>Go next</source>
        <translation>Перейти к следующей </translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="110"/>
        <source>Go last</source>
        <translation>Перейти к последней</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="116"/>
        <source>Inh.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="117"/>
        <source>Rec.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="118"/>
        <source>Comp.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="119"/>
        <source>Tun.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="124"/>
        <source>Tuned</source>
        <translation>Настроено</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="121"/>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="283"/>
        <source>Inharmonicity</source>
        <translation>Негармонично</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="122"/>
        <source>Recorded</source>
        <translation>Записанно</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="123"/>
        <source>Computed</source>
        <translation>Вычисленно</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="137"/>
        <source>Key index</source>
        <translation>Номер кавиши</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="287"/>
        <source>Frequency deviation [cent]</source>
        <translation>Отклонение частоты [cent]</translation>
    </message>
</context>
<context>
    <name>ProgressDisplay</name>
    <message>
        <location filename="../Qt/progressdisplay.cpp" line="56"/>
        <source>Synthesizer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="982"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="985"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="988"/>
        <source>Documents</source>
        <translation>Документы</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="993"/>
        <source>Images</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="1008"/>
        <source>Export File Name</source>
        <translation>Экспортировать файл с именем</translation>
    </message>
</context>
<context>
    <name>RecordingQualityBar</name>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="40"/>
        <source>Quality</source>
        <translation>Качество</translation>
    </message>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="41"/>
        <source>This bar displays the quality of the recording. All of the recorded keys should have an almost equal quality before starting the calculation.</source>
        <translation>Эта панель отображает качество записи. Все записанные клавиши должны иметь примерно одинаковый уровень качества перед началом вычислений.</translation>
    </message>
</context>
<context>
    <name>RecordingStatusGraphicsView</name>
    <message>
        <location filename="../Qt/recordingstatusgraphicsview.cpp" line="33"/>
        <source>This item displays the status of the recorder. A red circle indicates that the audio signal is currently recorded. A blue rotating circle is shown when the program processes the recorded signal. A green pause symbol is displayed if you can record the next key.</source>
        <translation>Эти символы отображают состояние записывающего устройства. Красный круг указывает на то, что аудиосигнал в настоящее время записывается. Синий круг вращающихся отображается, когда программа обрабатывает записанный сигнал. Зеленый символ паузы отображается, если вы можете записать следующую клавишу.</translation>
    </message>
</context>
<context>
    <name>SignalAnalyzerGroupBox</name>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="31"/>
        <source>Signal analyzer</source>
        <translation>Анализатор сигнала</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="68"/>
        <source>Hz</source>
        <translation>Гц</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="78"/>
        <source>This label displays the current selected key.</source>
        <translation>Этот знак отображает выбранную сейчас клавишу.</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="79"/>
        <source>This label shows the ground frequency of the selected key.</source>
        <translation>Этот знак показывает основную частоту выбранной клавиши.</translation>
    </message>
</context>
<context>
    <name>SimpleFileDialog</name>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="92"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="148"/>
        <source>File existing</source>
        <translation>Файл существует</translation>
    </message>
    <message>
        <source>A file with the given name already exits at %1. Do you want to overwrite it?</source>
        <translation type="vanished">Файл с заданным именем уже существует на %1. Вы хотите перезаписать его?</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="149"/>
        <source>A file with the given name already exists at %1. Do you want to overwrite it?</source>
        <translation>Файл с заданным именем уже существует на %1 пути. Вы хотите перезаписать его?</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Invalid filename</source>
        <translation>Неверное имя файла</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Please provide a valid filename.</source>
        <translation>Пожалуйста введите правельное имя файла.</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="178"/>
        <source>Remove file</source>
        <translation>Удалить файл</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="179"/>
        <source>Are you sure that you really want to delete the file &quot;%1&quot;?</source>
        <translation>Вы уверены, что Вы действительно хотите удалить файл &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>TunerApplication</name>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>Crash handler</source>
        <translation>Обработка ошибок</translation>
    </message>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>The application exited unexpectedly on the last run. Do you want to view the last log?</source>
        <translation>Последний раз приложение неожиданно завершило работу. Вы хотите посмотреть последнюю запись журнала?</translation>
    </message>
</context>
<context>
    <name>TuningGroupBox</name>
    <message>
        <source>Tuning</source>
        <translation type="vanished">Настройка</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorGroupBox</name>
    <message>
        <location filename="../Qt/tuningindicatorgroupbox.cpp" line="27"/>
        <source>Tuning</source>
        <translation type="unfinished">Настройка</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorView</name>
    <message>
        <location filename="../Qt/tuningindicatorview.cpp" line="46"/>
        <source>This is the tuning indicator. Touch this field to toggle between spectral and stroboscopic mode. In the spectral mode you should bring the peak to the center of the window for optimal tuning. When tuning several strings of unisons at once, several peaks might appear and all of them should be tuned to match the center. In the stroboscopic mode several stripes of drifiting rainbows are shown. The stripes represent the partials. Optimal tuning of single strings is obtained when the rainbows come to a halt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeControlGroupBox</name>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="37"/>
        <source>Volume control</source>
        <translation>Громкость</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="46"/>
        <source>Volume</source>
        <translation>Громкость</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="131"/>
        <source>Click this button to mute the speaker or headphone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="142"/>
        <source>This widgets provides settings and information about the input level of the input device.</source>
        <translation>Это виджеты показывает настройки и информацию о уровне входного сигнала входного устройства.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="143"/>
        <source>If the input level drops below this mark the recorder stops and does not process the input signal.</source>
        <translation>Если уровень входного сигнала падает ниже этой отметки, устройство остановливается и не обрабатывает входящий сигнал.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="144"/>
        <source>If the input level reaches this threshold the recorder starts analyzing the signal of the input device until the level drops below the &apos;Off&apos; mark.</source>
        <translation>Если уровень входного сигнала достигает этого порога устройство начинает анализировать входящий сигнал и анализирует до тех пор, пока уровень сигнала не опустится ниже отметки в &quot;Выкл.&quot;.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="145"/>
        <source>Click this button to reset the automatic calibration of the input volume.</source>
        <translation>Нажмите на эту кнопку для сброса автоматической калибрации входящего звука.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="146"/>
        <source>Click this button to mute the input device.</source>
        <translation>Нажмите на эту кнопку чтобы заглушить входящее устройство.</translation>
    </message>
</context>
<context>
    <name>VolumeControlLevel</name>
    <message>
        <location filename="../Qt/volumecontrollevel.cpp" line="32"/>
        <source>This bar displays the current input level.</source>
        <translation>Данная панель отображает текущий уровень входного сигнала.</translation>
    </message>
</context>
<context>
    <name>ZoomedSpectrumGraphicsView</name>
    <message>
        <source>This is the tuning device. You should bring the peak and the indicator bar in the middle of the window for an optimal tuning. When tuning several strings at once, there might appear several peaks. All of them should be tuned to match the center.</source>
        <translation type="vanished">Это устройство настройки. Вы должны принести пик и полоску индикатора в середину окна для оптимальной настройки. При настройке нескольких струн сразу, там может появиться несколько пиков.Все они должны быть так, чтобы были в центре.</translation>
    </message>
</context>
<context>
    <name>options::OptionsDialog</name>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="48"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="70"/>
        <source>Environment</source>
        <translation>Окружение</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="71"/>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
</context>
<context>
    <name>options::PageAudio</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="31"/>
        <source>Input device</source>
        <translation>Входящее устройство</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="32"/>
        <source>Output device</source>
        <translation>Исходящее устройство</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="33"/>
        <source>Midi</source>
        <translation>MIDI</translation>
    </message>
</context>
<context>
    <name>options::PageAudioInputOutput</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="64"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="70"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="95"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="111"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <source>Input device</source>
        <translation type="vanished">Входящее устройство</translation>
    </message>
    <message>
        <source>Sample rates</source>
        <translation type="vanished">Частоты дискретизации</translation>
    </message>
    <message>
        <source>Sampling rates</source>
        <translation type="vanished">Частота опроса</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="65"/>
        <source>Device</source>
        <translation>устройство</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="71"/>
        <source>Sampling rate </source>
        <translation>Частота дискретизации</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="77"/>
        <source>Open system settings</source>
        <translation>Открыть системные настройки</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="90"/>
        <source>Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="102"/>
        <source>Buffer size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>You need at least a sampling rate of %1 to record and play all keys.</source>
        <translation>Вы должны иметь хотя бы частоту дискретизации от %1 для записи и проигрывания всех клавиш.</translation>
    </message>
</context>
<context>
    <name>options::PageAudioMidi</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudiomidipage.cpp" line="33"/>
        <source>Midi device</source>
        <translation>MIDI устройство</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironment</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="29"/>
        <source>General</source>
        <translation>Основные</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="30"/>
        <source>Tuning</source>
        <translation>Настройка</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentGeneral</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="47"/>
        <source>User Interface</source>
        <translation>Пользовательский интерфейс</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="54"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="56"/>
        <source>&lt;System Language&gt;</source>
        <translation>&lt;Язык системы&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="83"/>
        <source>Reactivate warnings</source>
        <translation>Повторная активация предупреждений</translation>
    </message>
    <message>
        <source>Reset warnings</source>
        <translation type="vanished">Сбросить предупреждения</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="107"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="108"/>
        <source>The language change will take effect after a restart of the entropy piano tuner.</source>
        <translation>Изменение языка вступит в силу после перезапуска Энтропийного Тюнера для Фортепиано.</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentTuning</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="35"/>
        <source>Disabled</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="36"/>
        <source>Synthesized key sound</source>
        <translation>Синтезированный звук клавиши</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="37"/>
        <source>Reference key</source>
        <translation>Клавиша для сравнения</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="38"/>
        <source>Synthesizer mode</source>
        <translation>Режим синтезатора</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="42"/>
        <source>Dynamic synthesizer volume</source>
        <translation>Динамическая громкось синтезатора</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="48"/>
        <source>Stroboscopic tuning indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="54"/>
        <source>Disable automatic key selection</source>
        <translation>Отключить автоматический выбор кавиши</translation>
    </message>
</context>
</TS>
