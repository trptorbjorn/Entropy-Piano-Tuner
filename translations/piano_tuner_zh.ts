<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="49"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="58"/>
        <source>Entropy Piano Tuner</source>
        <translation>熵钢琴调律器</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="75"/>
        <source>Built on %1</source>
        <translation>建于 %1</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="76"/>
        <source>by %1 and %2</source>
        <translation>由 %1 y %2</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="78"/>
        <source>Based on</source>
        <translation>基于</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="89"/>
        <source>Copyright %1 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation>版权所有 %1 Würzburg大学物理理论系III。保留所有权利.</translation>
    </message>
    <message>
        <source>Copyright 2016 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="obsolete">版权所有 2015 Würzburg大学物理理论系III。保留所有权利. {2016 ?}</translation>
    </message>
    <message>
        <source>Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="vanished">版权所有 2015 Würzburg大学物理理论系III。保留所有权利.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="90"/>
        <source>This software is licensed under the terms of the %1. The source code can be accessed at %2.</source>
        <translation>本软件在 %1的条款内许可.源代码可以访问 %2.</translation>
    </message>
    <message>
        <source>This software is licensed unter the terms of the %1. The source code can be accessed at %2.</source>
        <translation type="vanished">本软件在 %1的条款内许可.源代码可以访问 %2.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="94"/>
        <source>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>该程序不提供任何形式的担保，包括设计、特殊用途的可销性和适用性的担保。</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="96"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>我们感谢所有为这个项目贡献的人:</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="120"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>AlgorithmDialog</name>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="83"/>
        <source>Algorithm:</source>
        <translation>算法:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="201"/>
        <source>Info of algorithm: %1</source>
        <translation>算法信息: %1</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="211"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="218"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="219"/>
        <source>Author:</source>
        <translation>作者:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="220"/>
        <source>Year:</source>
        <translation>年份:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="225"/>
        <source>Description:</source>
        <translation>描述:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="230"/>
        <source>Parameters</source>
        <translation>参数</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="383"/>
        <source>Default</source>
        <translation>缺省</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="385"/>
        <source>Reset the parameter to its default value</source>
        <translation>重置参数为默认值</translation>
    </message>
</context>
<context>
    <name>CalculationProgressGroup</name>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="69"/>
        <source>Status:</source>
        <translation>状态:</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="116"/>
        <source>Info</source>
        <translatorcomment>信息</translatorcomment>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="179"/>
        <source>An unknown error occured during the calculation.</source>
        <translation>计算中产生一个不明错误.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="182"/>
        <source>No data available.</source>
        <translation>无数据可用.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="185"/>
        <source>Not all keys recorded</source>
        <translation>不是所有键被录音</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="188"/>
        <source>Key data inconsistent.</source>
        <translation>键数据不一致.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="191"/>
        <source>Not enough keys recorded in left section.</source>
        <translation>左边没有足够键被录音</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="194"/>
        <source>Not enough keys recorded in right section.</source>
        <translation>右边没有足够键被录音</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="197"/>
        <source>Undefined error message.</source>
        <translation>无定义错误信息</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="202"/>
        <source>Calculation error</source>
        <translation>计算出错</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="203"/>
        <source>Error code</source>
        <translation>错误码</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="155"/>
        <source>Calculation started</source>
        <translation>计算已开始</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="156"/>
        <source>Stop calculation</source>
        <translation>停止计算</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>Calculation finished</source>
        <translation>计算完成</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>The calculation finished successfully! Now you can switch to the tuning mode and tune your piano.</source>
        <translation>计算成功完成！可以切换到调律模式并调整钢琴.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="170"/>
        <source>Minimizing the entropy</source>
        <translation>最小化熵</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="231"/>
        <source>Calculation with: %1</source>
        <translation>计算进度: %1</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="264"/>
        <source>Calculation canceled</source>
        <translation>计算中止</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="266"/>
        <location filename="../Qt/calculationprogressgroup.cpp" line="295"/>
        <source>Start calculation</source>
        <translation>开始计算</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="291"/>
        <source>Press the button to start the calculation</source>
        <translation>按此键开始计算</translation>
    </message>
</context>
<context>
    <name>DoNotShowAgainMessageBox</name>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="29"/>
        <source>Do not show again.</source>
        <translation>不再显示.</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="vanished">重置录音</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">重置音高标记</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="34"/>
        <source>Clear pitch markers</source>
        <translation>清除音高标记</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="38"/>
        <source>Not all keys recorded</source>
        <translation>不是所有键被录音</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="42"/>
        <source>Tuning curve not calculated</source>
        <translation>没有计算调律曲线</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="46"/>
        <source>Tuning curve must be recalculated</source>
        <translation>调律曲线需重新计算</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="50"/>
        <source>Save changes</source>
        <translation>保存更改</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="55"/>
        <source>Question</source>
        <translation>问题</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="64"/>
        <source>Do you really want to clear all pitch markers? This can not be undone!</source>
        <translation>你真的要清除所有音高标记？这不能恢复！</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="76"/>
        <source>Do you want to save your current changes? You can save at any time using the tool button or the action from the menu.</source>
        <translation>你要保存当前更改？您可以使用工具按钮或从菜单中选择操作随时保存。</translation>
    </message>
    <message>
        <source>Do you really want to reset all pitch markers? This can not be undone!</source>
        <translatorcomment>added translation by Google, please refine</translatorcomment>
        <translation type="vanished">你真的要重置所有音高标记？这不能逆转！</translation>
    </message>
    <message>
        <source>Do you really want to reset all recorded keys? This can not be made undone!</source>
        <translation type="vanished">确信要重置所有已录音的键吗？这是不能复原的!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="67"/>
        <source>Not all keys have been recorded. Switch the mode and record them.</source>
        <translation>并不是所有的键都被录音，切换模式并录音.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="70"/>
        <source>The tuning curve has not been calculated. Switch the mode and calculate it.</source>
        <translation>调律曲线没有被计算.切换到模式并计算.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="73"/>
        <source>There are missing frequencies in the calculated tuning curve. Recalculate to fix this.</source>
        <translation>计算调律曲线时丢失频率.再次计算修复它。</translation>
    </message>
</context>
<context>
    <name>EditPianoSheetDialog</name>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="14"/>
        <source>Piano data sheet</source>
        <translation>钢琴数据表</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="35"/>
        <source>Piano operating site information</source>
        <translation>钢琴现场作业信息</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="44"/>
        <source>Tuning location</source>
        <translation>调律地点</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="54"/>
        <source>Time of tuning</source>
        <translation>调律时间</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="82"/>
        <source>Now</source>
        <translation>现在</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="91"/>
        <source>Concert pitch</source>
        <translation>音乐会音高</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="106"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="128"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="230"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="264"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="322"/>
        <source>Default</source>
        <translation>缺省</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="140"/>
        <source>Piano manufacturer information</source>
        <translation>钢琴制造商信息</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="149"/>
        <source>Piano name</source>
        <translation>钢琴品牌</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="159"/>
        <source>Serial number</source>
        <translation>序列号</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="169"/>
        <source>Manufaction year</source>
        <translation>制造年份</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="195"/>
        <source>Production location</source>
        <translation>产地</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="205"/>
        <source>Number of keys</source>
        <translation>键数目</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="239"/>
        <source>Key number of A</source>
        <translation>A的键号</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="273"/>
        <source>Piano type</source>
        <translation>钢琴类型</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="287"/>
        <source>Grand</source>
        <translation>卧式</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="292"/>
        <source>Upright</source>
        <translation>立式</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="300"/>
        <source>Keys on bass bridge</source>
        <translatorcomment>低音琴桥上的键数</translatorcomment>
        <translation>低桥键数</translation>
    </message>
</context>
<context>
    <name>FullScreenKeyboardDialog</name>
    <message>
        <location filename="../Qt/keyboard/fullscreenkeyboarddialog.cpp" line="30"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
</context>
<context>
    <name>InitializeDialog</name>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="69"/>
        <source>Initializing the core component</source>
        <translation>初始化核心部件</translation>
    </message>
    <message>
        <source>Preparing</source>
        <translation type="vanished">准备</translation>
    </message>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="98"/>
        <source>Initializing, please wait</source>
        <translation>初始化，请等待</translation>
    </message>
    <message>
        <source>Initializing the midi component</source>
        <translation type="vanished">初始化midi部件</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">已完成</translation>
    </message>
</context>
<context>
    <name>KeyboardGraphicsView</name>
    <message>
        <location filename="../Qt/keyboard/keyboardgraphicsview.cpp" line="78"/>
        <source>This window displays the keyboard. Each key has a small indicator that will display the current recording state of this key.</source>
        <translation>这个窗口显示键盘。每个键都有一个小的指示器，该指示器将显示这个键的当前录音状态.</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../Qt/logviewer.ui" line="20"/>
        <location filename="../Qt/logviewer.ui" line="36"/>
        <source>Log</source>
        <translation>日志</translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="56"/>
        <source>Copy the contents of the log to the clipboard.</source>
        <translation>复制日志内容到剪贴板。
</translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="59"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Qt/mainwindow.ui" line="14"/>
        <location filename="../Qt/mainwindow.cpp" line="545"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="188"/>
        <source>Entropy piano tuner</source>
        <translation>熵钢琴调律器</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This windows displays the tuning curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这个窗口显示调律曲线.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This window displays the recorded spectrum of a single note. Bars will indicate the peaks that were found during the analysis.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这个窗口显示单个音符的记录频谱，竖条中将显示在分析过程中发现的峰。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="76"/>
        <source>&amp;File</source>
        <translation>&amp;文件</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="89"/>
        <source>&amp;Tools</source>
        <translation>&amp;工具</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="98"/>
        <source>&amp;Help</source>
        <translation>&amp;帮助</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="128"/>
        <source>&amp;Open</source>
        <translation>&amp;打开</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="133"/>
        <source>&amp;Save</source>
        <translation>&amp;保存</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="138"/>
        <source>E&amp;xit</source>
        <translation>&amp;退出</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="143"/>
        <location filename="../Qt/mainwindow.cpp" line="210"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="148"/>
        <source>Open sound control</source>
        <translation>打开声音控制</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="153"/>
        <source>Save &amp;As</source>
        <translation>另存 &amp;为</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="158"/>
        <source>&amp;New</source>
        <translation>&amp;新建</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="163"/>
        <source>&amp;Edit piano data sheet</source>
        <translation>&amp;编辑钢琴数据表</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="173"/>
        <source>Share</source>
        <translation>共享</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="181"/>
        <source>View log</source>
        <translation>查看日志</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="186"/>
        <source>&amp;Options</source>
        <translation>&amp;选项</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="191"/>
        <source>&amp;Clear pitch markers</source>
        <translation>&amp;清除音高标记</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="194"/>
        <location filename="../Qt/mainwindow.cpp" line="190"/>
        <source>Clear pitch markers</source>
        <translation>清除音高标记</translation>
    </message>
    <message>
        <source>&amp;Reset pitch markers</source>
        <translation type="vanished">&amp;重置音高标记</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">重置音高标记</translation>
    </message>
    <message>
        <source>&amp;Reset recording</source>
        <translation type="vanished">重置录音</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="vanished">重置录音</translation>
    </message>
    <message>
        <source>&amp;Reset recoding</source>
        <translation type="vanished">&amp;重置录音</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="199"/>
        <source>Manual</source>
        <translation>手册</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="204"/>
        <location filename="../Qt/mainwindow.cpp" line="208"/>
        <source>Tutorial</source>
        <translation>教程</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="143"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="149"/>
        <source>Record</source>
        <translation>录音</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="155"/>
        <source>Calculate</source>
        <translation>计算</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="161"/>
        <source>Tune</source>
        <translation>调律</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="145"/>
        <source>Press this button to switch to the idle mode.</source>
        <translation>按此钮切换到空闲模式.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="151"/>
        <source>Press this button to switch to the recording mode.</source>
        <translation>按此钮切换到录音模式.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="157"/>
        <source>Press this button to switch to the calculation mode.</source>
        <translation>按此钮切换到计算模式.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="163"/>
        <source>Press this button to switch to the tuning mode.</source>
        <translation>按此钮切换到调律模式.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="179"/>
        <source>Documents and tools</source>
        <translation>文档和工具</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="185"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="188"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="189"/>
        <source>Edit piano data sheet</source>
        <translation>编辑钢琴数据表</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="192"/>
        <source>Options</source>
        <translatorcomment>选项</translatorcomment>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="193"/>
        <source>Graphs</source>
        <translation>图</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="195"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>Log</source>
        <translation type="vanished">日志</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="230"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="371"/>
        <source>File created</source>
        <translation>文件已建立</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="374"/>
        <source>File edited</source>
        <translation>文件已编辑</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="377"/>
        <source>File opened</source>
        <translation>文件已打开</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="380"/>
        <source>File saved</source>
        <translation>文件已保存</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="458"/>
        <source>Recording keystroke</source>
        <translation>录音击键</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="466"/>
        <source>Signal analysis started</source>
        <translation>信号分析已开始</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="469"/>
        <source>Signal analysis ended</source>
        <translation>信号分析已结束</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="498"/>
        <source>Calculation failed</source>
        <translation>计算失败</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="501"/>
        <source>Calculation ended</source>
        <translation>计算已结束</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="504"/>
        <source>Calculation started</source>
        <translation>计算已开始</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>Warning</source>
        <translation>告警</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <source>Unable to find a supported sound control.</source>
        <translation>无法找到一个可支持的声音控制.</translation>
    </message>
    <message>
        <source>Unable to find a supported sound conrol.</source>
        <translation type="vanished">无法找到一个可支持的声音控制.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>Canceled</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>You need to save the file before you can share it.</source>
        <translation>在你共享文件之前需要保存文件o</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="788"/>
        <source>A new update is available!</source>
        <translation>一个新的更新可用！</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="789"/>
        <source>The online app version is %1. Do you want to install this update?</source>
        <translation>在线应用程序的版本是 %1.您想安装此更新吗？</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>The maintenance tool could not be started automatically. To update the program you have to start the maintenance tool manually.</source>
        <translation>无法自动启动维护工具。您必须手动升级程序。</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="71"/>
        <source>The document has been modified.</source>
        <translation>文档已被修改.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="72"/>
        <source>Do you want to save your changes?</source>
        <translation>要保存你的更改?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="187"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="95"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="186"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="119"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="137"/>
        <source>Share tuning data</source>
        <translation>共享调律数据</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="154"/>
        <source>New piano</source>
        <translation>新钢琴</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="157"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="159"/>
        <source>Unknown</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>Error</source>
        <translation>出错</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <source>File could not be opened.</source>
        <translation>无法打开文件</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>File could not be saved.</source>
        <translation>无法保存文件.</translation>
    </message>
    <message>
        <source>Entopy piano tuner</source>
        <translation type="obsolete">熵钢琴调律器</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="191"/>
        <source>Comma-separated values</source>
        <translation>逗号分隔的数值</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="194"/>
        <source>All files</source>
        <translation>所有文件</translation>
    </message>
</context>
<context>
    <name>PlotsDialog</name>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="106"/>
        <source>Reset view</source>
        <translation>重置视图</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="107"/>
        <source>Go first</source>
        <translatorcomment>先走</translatorcomment>
        <translation>到开始</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="108"/>
        <source>Go previous</source>
        <translation>上一页</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="109"/>
        <source>Go next</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="110"/>
        <source>Go last</source>
        <translation>到最后</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="116"/>
        <source>Inh.</source>
        <translatorcomment>失谐度</translatorcomment>
        <translation>失谐度</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="117"/>
        <source>Rec.</source>
        <translatorcomment>录制的</translatorcomment>
        <translation>录制</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="118"/>
        <source>Comp.</source>
        <translatorcomment>计算</translatorcomment>
        <translation>计算</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="119"/>
        <source>Tun.</source>
        <translatorcomment>调律</translatorcomment>
        <translation>调律</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="124"/>
        <source>Tuned</source>
        <translatorcomment>调律</translatorcomment>
        <translation>调律</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="121"/>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="283"/>
        <source>Inharmonicity</source>
        <translation>失谐度</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="122"/>
        <source>Recorded</source>
        <translation>录音</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="123"/>
        <source>Computed</source>
        <translation>计算</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="137"/>
        <source>Key index</source>
        <translation>键索引</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="287"/>
        <source>Frequency deviation [cent]</source>
        <translation>频率偏差[音分]</translation>
    </message>
</context>
<context>
    <name>ProgressDisplay</name>
    <message>
        <location filename="../Qt/progressdisplay.cpp" line="56"/>
        <source>Synthesizer</source>
        <translation>合成器</translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="982"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="985"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="988"/>
        <source>Documents</source>
        <translation>文档</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="993"/>
        <source>Images</source>
        <translation>图像</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="1008"/>
        <source>Export File Name</source>
        <translation>导出文件名</translation>
    </message>
</context>
<context>
    <name>RecordingQualityBar</name>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="40"/>
        <source>Quality</source>
        <translation>质量</translation>
    </message>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="41"/>
        <source>This bar displays the quality of the recording. All of the recorded keys should have an almost equal quality before starting the calculation.</source>
        <translation>此条显示录音质量。开始前计算所有已录音的键应该有一个几乎相等的质量.</translation>
    </message>
</context>
<context>
    <name>RecordingStatusGraphicsView</name>
    <message>
        <location filename="../Qt/recordingstatusgraphicsview.cpp" line="33"/>
        <source>This item displays the status of the recorder. A red circle indicates that the audio signal is currently recorded. A blue rotating circle is shown when the program processes the recorded signal. A green pause symbol is displayed if you can record the next key.</source>
        <translation>该项显示录音器的状态。一个红色圆圈表示音频信号是当前录音的。一个蓝色旋转圆表示程序处理已录信号。如果你能录下一个键，则显示绿色的暂停符号.</translation>
    </message>
</context>
<context>
    <name>SignalAnalyzerGroupBox</name>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="31"/>
        <source>Signal analyzer</source>
        <translation>信号分析器</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="68"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="78"/>
        <source>This label displays the current selected key.</source>
        <translation>此标签显示当前选定的键.</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="79"/>
        <source>This label shows the ground frequency of the selected key.</source>
        <translation>此标签显示当前选定键的基频.</translation>
    </message>
</context>
<context>
    <name>SimpleFileDialog</name>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="92"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="148"/>
        <source>File existing</source>
        <translation>文件存在</translation>
    </message>
    <message>
        <source>A file with the given name already exits at %1. Do you want to overwrite it?</source>
        <translation type="vanished">给定名称的文件已存在 %1.你想复盖吗？</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="149"/>
        <source>A file with the given name already exists at %1. Do you want to overwrite it?</source>
        <translation>给定文件名已存在 %1.你想复盖吗？</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Invalid filename</source>
        <translation>无效文件名</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Please provide a valid filename.</source>
        <translation>请提供有效文件名.</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="178"/>
        <source>Remove file</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="179"/>
        <source>Are you sure that you really want to delete the file &quot;%1&quot;?</source>
        <translation>你确定要删除文件 &quot;%1&quot;吗？</translation>
    </message>
</context>
<context>
    <name>TunerApplication</name>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>Crash handler</source>
        <translation>系统崩溃处理</translation>
    </message>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>The application exited unexpectedly on the last run. Do you want to view the last log?</source>
        <translation>应用程序在最后一次运行时意外退出。你想查看最后一个日志吗？</translation>
    </message>
</context>
<context>
    <name>TuningGroupBox</name>
    <message>
        <source>Tuning</source>
        <translation type="vanished">调律</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorGroupBox</name>
    <message>
        <location filename="../Qt/tuningindicatorgroupbox.cpp" line="27"/>
        <source>Tuning</source>
        <translation>调律</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorView</name>
    <message>
        <location filename="../Qt/tuningindicatorview.cpp" line="46"/>
        <source>This is the tuning indicator. Touch this field to toggle between spectral and stroboscopic mode. In the spectral mode you should bring the peak to the center of the window for optimal tuning. When tuning several strings of unisons at once, several peaks might appear and all of them should be tuned to match the center. In the stroboscopic mode several stripes of drifiting rainbows are shown. The stripes represent the partials. Optimal tuning of single strings is obtained when the rainbows come to a halt.</source>
        <translation>这是调律指示器。轻触此区域，可以在频谱和频闪模式之间切换。在频谱模式下，把波峰带到窗口中心为最佳调整。当一次调整几条弦的同度时，会出现几个波峰，它们都应该调整到中心位置。在频闪模式下会显示数条移动的彩虹条纹。每条条纹代表一个谐音。当彩虹条静止时得到单弦的最优调整。</translation>
    </message>
</context>
<context>
    <name>VolumeControlGroupBox</name>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="37"/>
        <source>Volume control</source>
        <translation>音量控制</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="46"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="131"/>
        <source>Click this button to mute the speaker or headphone.</source>
        <translation>点击此按钮使扬声器或耳机静音</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="142"/>
        <source>This widgets provides settings and information about the input level of the input device.</source>
        <translation>此部件提供输入设备信息和输入电平设置。.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="143"/>
        <source>If the input level drops below this mark the recorder stops and does not process the input signal.</source>
        <translation>如果输入电平下降到下面这个标记的话，录音器会停止并不处理输入信号。</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="144"/>
        <source>If the input level reaches this threshold the recorder starts analyzing the signal of the input device until the level drops below the &apos;Off&apos; mark.</source>
        <translation>如果输入电平达到这个阈值，录音器开始分析输入设备的信号，直到电平降到 &apos;Off&apos;标记.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="145"/>
        <source>Click this button to reset the automatic calibration of the input volume.</source>
        <translation>单击此按钮可重置输入音量的自动校准。</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="146"/>
        <source>Click this button to mute the input device.</source>
        <translation>请按此钮使输入设备静音。</translation>
    </message>
</context>
<context>
    <name>VolumeControlLevel</name>
    <message>
        <location filename="../Qt/volumecontrollevel.cpp" line="32"/>
        <source>This bar displays the current input level.</source>
        <translation>这条块显示当前输入电平.</translation>
    </message>
</context>
<context>
    <name>ZoomedSpectrumGraphicsView</name>
    <message>
        <source>This is the tuning device. You should bring the peak and the indicator bar in the middle of the window for an optimal tuning. When tuning several strings at once, there might appear several peaks. All of them should be tuned to match the center.</source>
        <translation type="vanished">这是调律器，带波峰和指示条到窗口中心为优化调整，当一次调整几根弦时，可能会出现几个峰值，所有这些峰都应该被调到匹配的中心。</translation>
    </message>
</context>
<context>
    <name>options::OptionsDialog</name>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="48"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="70"/>
        <source>Environment</source>
        <translation>环境</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="71"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
</context>
<context>
    <name>options::PageAudio</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="31"/>
        <source>Input device</source>
        <translation>输入设备</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="32"/>
        <source>Output device</source>
        <translation>输出设备</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="33"/>
        <source>Midi</source>
        <translation>Midi</translation>
    </message>
</context>
<context>
    <name>options::PageAudioInputOutput</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="64"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="70"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="95"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="111"/>
        <source>Default</source>
        <translation>缺省</translation>
    </message>
    <message>
        <source>Input device</source>
        <translation type="vanished">输入设备</translation>
    </message>
    <message>
        <source>Sample rates</source>
        <translation type="vanished">采样率</translation>
    </message>
    <message>
        <source>Sampling rates</source>
        <translation type="vanished">采样率</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="65"/>
        <source>Device</source>
        <translation>设备</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="71"/>
        <source>Sampling rate </source>
        <translation>采样率</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="77"/>
        <source>Open system settings</source>
        <translation>打开系统设置</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="90"/>
        <source>Channels</source>
        <translation>通道数</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="102"/>
        <source>Buffer size</source>
        <translation>缓冲区大小</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>You need at least a sampling rate of %1 to record and play all keys.</source>
        <translation>对所有键录音需要至少 %1的采样率.</translation>
    </message>
</context>
<context>
    <name>options::PageAudioMidi</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudiomidipage.cpp" line="33"/>
        <source>Midi device</source>
        <translation>MIDI设备</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironment</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="29"/>
        <source>General</source>
        <translation>通用</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="30"/>
        <source>Tuning</source>
        <translation>调律</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentGeneral</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="47"/>
        <source>User Interface</source>
        <translation>用户接口</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="54"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="56"/>
        <source>&lt;System Language&gt;</source>
        <translation>&lt;系统语言&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="83"/>
        <source>Reactivate warnings</source>
        <translation>重新激活警告</translation>
    </message>
    <message>
        <source>Reset warnings</source>
        <translation type="vanished">重置警告</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="107"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="108"/>
        <source>The language change will take effect after a restart of the entropy piano tuner.</source>
        <translation>语言改变必须重启熵钢琴调律器后生效.</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentTuning</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="35"/>
        <source>Disabled</source>
        <translation>不可用</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="36"/>
        <source>Synthesized key sound</source>
        <translation>合成键声音</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="37"/>
        <source>Reference key</source>
        <translation>参考键</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="38"/>
        <source>Synthesizer mode</source>
        <translation>合成器模式</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="42"/>
        <source>Dynamic synthesizer volume</source>
        <translation>动态合成器音量</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="48"/>
        <source>Stroboscopic tuning indicator</source>
        <translation>频闪指示器</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="54"/>
        <source>Disable automatic key selection</source>
        <translation>禁止自动键选择</translation>
    </message>
</context>
</TS>
