<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="49"/>
        <source>About</source>
        <translation>Informacje o EPT</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="58"/>
        <source>Entropy Piano Tuner</source>
        <translation>Entropy Piano Tuner</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="75"/>
        <source>Built on %1</source>
        <translation>Utworzono: %1</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="76"/>
        <source>by %1 and %2</source>
        <translation>przez %1 oraz %2</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="78"/>
        <source>Based on</source>
        <translation>Oparte na</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="89"/>
        <source>Copyright %1 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation>Copyright %1 Dept. of Theor. Phys. III, University of Würzburg. Wszelkie prawa zastrzeżone.</translation>
    </message>
    <message>
        <source>Copyright 2016 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="obsolete">Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. Wszelkie prawa zastrzeżone. {2016 ?}</translation>
    </message>
    <message>
        <source>Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. All rights reserved.</source>
        <translation type="vanished">Copyright 2015 Dept. of Theor. Phys. III, University of Würzburg. Wszelkie prawa zastrzeżone.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="90"/>
        <source>This software is licensed under the terms of the %1. The source code can be accessed at %2.</source>
        <translation>To oprogramowanie jest licencjonowane na zasadach licencji %1. Kod źródłowy jest dostępny w %2.</translation>
    </message>
    <message>
        <source>This software is licensed unter the terms of the %1. The source code can be accessed at %2.</source>
        <translation type="vanished">Ten program jest licencjonowany na warunkach %1. Kod źródłowy jest dostępny pod adresem %2.</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="94"/>
        <source>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="96"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Dziękujemy wszystkim tym, którzy pomogli przy projekcie:</translation>
    </message>
    <message>
        <location filename="../Qt/aboutdialog.cpp" line="120"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>AlgorithmDialog</name>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="83"/>
        <source>Algorithm:</source>
        <translation>Algorytm:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="201"/>
        <source>Info of algorithm: %1</source>
        <translation>Informacje na temat algorytmu: %1</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="211"/>
        <source>Info</source>
        <translation>Informacje</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="218"/>
        <source>Name:</source>
        <translation>Nazwa:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="219"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="220"/>
        <source>Year:</source>
        <translation>Rok:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="225"/>
        <source>Description:</source>
        <translation>Opis:</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="230"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="383"/>
        <source>Default</source>
        <translation>Domyślne</translation>
    </message>
    <message>
        <location filename="../Qt/algorithmdialog/algorithmdialog.cpp" line="385"/>
        <source>Reset the parameter to its default value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalculationProgressGroup</name>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="69"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="116"/>
        <source>Info</source>
        <translation>Informacje</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="179"/>
        <source>An unknown error occured during the calculation.</source>
        <translation>Podczas obliczeń wystąpił nieznany błąd.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="182"/>
        <source>No data available.</source>
        <translation>Brak danych.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="185"/>
        <source>Not all keys recorded</source>
        <translation>Nie wszystkie klawisze zostały nagrane</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="188"/>
        <source>Key data inconsistent.</source>
        <translation>Dane klawiszy niekonsystentne.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="191"/>
        <source>Not enough keys recorded in left section.</source>
        <translation>Nie ma wystarczającej liczby klawiszy nagrane (bas).</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="194"/>
        <source>Not enough keys recorded in right section.</source>
        <translation>Nie ma wystarczającej liczby klawiszy rejestrowana (soprany).</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="197"/>
        <source>Undefined error message.</source>
        <translation>Niezdefiniowany komunikat o błędzie.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="202"/>
        <source>Calculation error</source>
        <translation>Błąd podczas obliczeń</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="203"/>
        <source>Error code</source>
        <translation>Kod błędu</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="155"/>
        <source>Calculation started</source>
        <translation>Obliczenia rozpoczęte</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="156"/>
        <source>Stop calculation</source>
        <translation>Zatrzymaj obliczenia</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>Calculation finished</source>
        <translation>Obliczenia zakończone</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="162"/>
        <source>The calculation finished successfully! Now you can switch to the tuning mode and tune your piano.</source>
        <translation>Obliczenia zakończone sukcesem! Teraz możesz przejść do trybu strojenia by nastroić swój instrument.</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="170"/>
        <source>Minimizing the entropy</source>
        <translation>Minimalizuję entropię</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="231"/>
        <source>Calculation with: %1</source>
        <translation>Obliczenia z: %1</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="264"/>
        <source>Calculation canceled</source>
        <translation>Obliczenia przerwane</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="266"/>
        <location filename="../Qt/calculationprogressgroup.cpp" line="295"/>
        <source>Start calculation</source>
        <translation>Rozpocznij obliczenia</translation>
    </message>
    <message>
        <location filename="../Qt/calculationprogressgroup.cpp" line="291"/>
        <source>Press the button to start the calculation</source>
        <translation>Naciśnij przycisk by rozpocząć obliczenia</translation>
    </message>
</context>
<context>
    <name>DoNotShowAgainMessageBox</name>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="29"/>
        <source>Do not show again.</source>
        <translation>Nie pokazuj ponownie.</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="vanished">Zresetuj nagrania</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">Zresetować znaczniki tonowe</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="34"/>
        <source>Clear pitch markers</source>
        <translation>Zresetować znaczniki tonowe</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="38"/>
        <source>Not all keys recorded</source>
        <translation>Nie wszystkie klawisze zostały nagrane</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="42"/>
        <source>Tuning curve not calculated</source>
        <translation>Krzywa strojenia niewyznaczona</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="46"/>
        <source>Tuning curve must be recalculated</source>
        <translation>Kryzwa strojenia musi zostać wyznaczona ponownie</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="50"/>
        <source>Save changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="55"/>
        <source>Question</source>
        <translation>Pytanie</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="64"/>
        <source>Do you really want to clear all pitch markers? This can not be undone!</source>
        <translation>Czy na pewno chcesz zresetować wszystkie znaczniki wysokości tonu? Ta operacja nie może zostać cofnięta!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="76"/>
        <source>Do you want to save your current changes? You can save at any time using the tool button or the action from the menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to reset all pitch markers? This can not be undone!</source>
        <translation type="vanished">Czy na pewno chcesz zresetować wszystkie znaczniki wysokości tonu? Ta operacja nie może zostać cofnięta!</translation>
    </message>
    <message>
        <source>Do you really want to reset all recorded keys? This can not be made undone!</source>
        <translation type="vanished">Czy naprawdę chcesz zresetować wszystkie nagrania klawiszy? Ta operacja nie może zostać cofnięta!</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="67"/>
        <source>Not all keys have been recorded. Switch the mode and record them.</source>
        <translation>Nie wszystkie klawisze zostały nagrane. Zmień tryb i nagraj je.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="70"/>
        <source>The tuning curve has not been calculated. Switch the mode and calculate it.</source>
        <translation>Krzywa strojenia nie została wyznaczona. Zmień tryb i rozpocznij obliczenia.</translation>
    </message>
    <message>
        <location filename="../Qt/donotshowagainmessagebox.cpp" line="73"/>
        <source>There are missing frequencies in the calculated tuning curve. Recalculate to fix this.</source>
        <translation>Obliczona krzywa strojenia nie zawiera wszystkich częstotliwości. Oblicz ponownie by to naprawić.</translation>
    </message>
</context>
<context>
    <name>EditPianoSheetDialog</name>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="14"/>
        <source>Piano data sheet</source>
        <translation>Dane instrumentu</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="35"/>
        <source>Piano operating site information</source>
        <translation>Dane miejsca działania instrumentu</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="44"/>
        <source>Tuning location</source>
        <translation>Miejsce strojenia</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="54"/>
        <source>Time of tuning</source>
        <translation>Czas strojenia</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="82"/>
        <source>Now</source>
        <translation>Teraz</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="91"/>
        <source>Concert pitch</source>
        <translation>Ton wzorcowy</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="106"/>
        <source>A</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="128"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="230"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="264"/>
        <location filename="../Qt/editpianosheetdialog.ui" line="322"/>
        <source>Default</source>
        <translation>Domyślne</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="140"/>
        <source>Piano manufacturer information</source>
        <translation>Informacje producenta</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="149"/>
        <source>Piano name</source>
        <translation>Nazwa instrumentu</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="159"/>
        <source>Serial number</source>
        <translation>Numer seryjny</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="169"/>
        <source>Manufaction year</source>
        <translation>Rok produkcji</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="195"/>
        <source>Production location</source>
        <translation>Miejsce produkcji</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="205"/>
        <source>Number of keys</source>
        <translation>Liczba klawiszy</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="239"/>
        <source>Key number of A</source>
        <translation>Numer klawisza a</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="273"/>
        <source>Piano type</source>
        <translation>Typ instrumentu</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="287"/>
        <source>Grand</source>
        <translation>Fortepian</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="292"/>
        <source>Upright</source>
        <translation>Pianino</translation>
    </message>
    <message>
        <location filename="../Qt/editpianosheetdialog.ui" line="300"/>
        <source>Keys on bass bridge</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FullScreenKeyboardDialog</name>
    <message>
        <location filename="../Qt/keyboard/fullscreenkeyboarddialog.cpp" line="30"/>
        <source>Keyboard</source>
        <translation>Klawiatura</translation>
    </message>
</context>
<context>
    <name>InitializeDialog</name>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="69"/>
        <source>Initializing the core component</source>
        <translation>Inicjalizacja komponentu głównego</translation>
    </message>
    <message>
        <source>Preparing</source>
        <translation type="vanished">Przygotowywanie</translation>
    </message>
    <message>
        <location filename="../Qt/initializedialog.cpp" line="98"/>
        <source>Initializing, please wait</source>
        <translation>Inicjalizacja, proszę czekać</translation>
    </message>
    <message>
        <source>Initializing the midi component</source>
        <translation type="vanished">Inicjalizacja komponentu midi</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">Ukończony</translation>
    </message>
</context>
<context>
    <name>KeyboardGraphicsView</name>
    <message>
        <location filename="../Qt/keyboard/keyboardgraphicsview.cpp" line="78"/>
        <source>This window displays the keyboard. Each key has a small indicator that will display the current recording state of this key.</source>
        <translation>To okno wyświetla klawiaturę. Każdy klawisz zawiera mały wskaźnik, który wyświetli aktualny stan tego klawisza.</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../Qt/logviewer.ui" line="20"/>
        <location filename="../Qt/logviewer.ui" line="36"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="56"/>
        <source>Copy the contents of the log to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/logviewer.ui" line="59"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Qt/mainwindow.ui" line="14"/>
        <location filename="../Qt/mainwindow.cpp" line="545"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="188"/>
        <source>Entropy piano tuner</source>
        <translation>Entropy Piano Tuner</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This windows displays the tuning curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To okno wyświetla krzywą strojenia.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This window displays the recorded spectrum of a single note. Bars will indicate the peaks that were found during the analysis.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To okno wyświetla zebrane spektrum pojedynczej nuty. Paski wskazują na maksima znalezione podczas analizy.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="76"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="89"/>
        <source>&amp;Tools</source>
        <translation>&amp;Narzędzia</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="98"/>
        <source>&amp;Help</source>
        <translation>P&amp;omoc</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="128"/>
        <source>&amp;Open</source>
        <translation>&amp;Otwórz</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="133"/>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="138"/>
        <source>E&amp;xit</source>
        <translation>Za&amp;kończ</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="143"/>
        <location filename="../Qt/mainwindow.cpp" line="210"/>
        <source>About</source>
        <translation>Informacje o EPT</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="148"/>
        <source>Open sound control</source>
        <translation>Otwórz ustawienia głośności</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="153"/>
        <source>Save &amp;As</source>
        <translation>Zapisz &amp;jako</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="158"/>
        <source>&amp;New</source>
        <translation>&amp;Nowy</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="163"/>
        <source>&amp;Edit piano data sheet</source>
        <translation>&amp;Edytuj dane pianina</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="173"/>
        <source>Share</source>
        <translation>Współdzielenie</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="181"/>
        <source>View log</source>
        <translation>Zobacz log</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="186"/>
        <source>&amp;Options</source>
        <translation>&amp;Opcje</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="191"/>
        <source>&amp;Clear pitch markers</source>
        <translation>&amp;Zresetować znaczniki tonowe</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="194"/>
        <location filename="../Qt/mainwindow.cpp" line="190"/>
        <source>Clear pitch markers</source>
        <translation>Zresetować znaczniki tonowe</translation>
    </message>
    <message>
        <source>&amp;Reset pitch markers</source>
        <translation type="vanished">&amp;Zresetować znaczniki tonowe</translation>
    </message>
    <message>
        <source>Reset pitch markers</source>
        <translation type="vanished">Zresetować znaczniki tonowe</translation>
    </message>
    <message>
        <source>Reset recording</source>
        <translation type="obsolete">Zresetuj nagrania</translation>
    </message>
    <message>
        <source>&amp;Reset recoding</source>
        <translation type="vanished">&amp;Resetuj nagrania</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="199"/>
        <source>Manual</source>
        <translation>Podręcznik użytkownika</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.ui" line="204"/>
        <location filename="../Qt/mainwindow.cpp" line="208"/>
        <source>Tutorial</source>
        <translation>Tutorial</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="143"/>
        <source>Idle</source>
        <translation>Nieaktywny</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="145"/>
        <source>Press this button to switch to the idle mode.</source>
        <translation>Naciśnij ten przycisk by przełączyć w tryb nieaktywny.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="149"/>
        <source>Record</source>
        <translation>Nagrywaj</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="151"/>
        <source>Press this button to switch to the recording mode.</source>
        <translation>Naciśnij ten przycik by przejść do trybu nagrywania.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="155"/>
        <source>Calculate</source>
        <translation>Oblicz</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="157"/>
        <source>Press this button to switch to the calculation mode.</source>
        <translation>Naciśnij ten przycsk by przełączyć w tryb obliczeń.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="161"/>
        <source>Tune</source>
        <translation>Strojenie</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="163"/>
        <source>Press this button to switch to the tuning mode.</source>
        <translation>Naciśnij ten przycsk by przełączyć w tryb strojenia.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="179"/>
        <source>Documents and tools</source>
        <translation>Dokumenty i narzędzia</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="185"/>
        <source>New</source>
        <translation>Nowy</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="188"/>
        <source>Save as</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="189"/>
        <source>Edit piano data sheet</source>
        <translation>Edytuj dane pianina</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="192"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="193"/>
        <source>Graphs</source>
        <translation>Wykresy</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="195"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Log</source>
        <translation type="vanished">Log</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="230"/>
        <source>Export</source>
        <translation>Eksport</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="371"/>
        <source>File created</source>
        <translation>Plik został utworzony</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="374"/>
        <source>File edited</source>
        <translation>Plik został zmieniony</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="377"/>
        <source>File opened</source>
        <translation>Plik został otwarty</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="380"/>
        <source>File saved</source>
        <translation>Plik został zapisany</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="458"/>
        <source>Recording keystroke</source>
        <translation>Nagrywanie stuknięcia klawisza</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="466"/>
        <source>Signal analysis started</source>
        <translation>Analiza sygnału rozpoczęta</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="469"/>
        <source>Signal analysis ended</source>
        <translation>Analiza sygnału zakończona</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="498"/>
        <source>Calculation failed</source>
        <translation>Wystąpił błąd podczas obliczeń</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="501"/>
        <source>Calculation ended</source>
        <translation>Obliczenia zakończone</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="504"/>
        <source>Calculation started</source>
        <translation>Obliczenia rozpoczęte</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="585"/>
        <source>Unable to find a supported sound control.</source>
        <translation>Nie udało się znaleźć systemowych ustawień głośności.</translation>
    </message>
    <message>
        <source>Unable to find a supported sound conrol.</source>
        <translation type="vanished">Nie udało się znaleźć systemowych ustawień głośności.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>Canceled</source>
        <translation>Anulowano</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="633"/>
        <source>You need to save the file before you can share it.</source>
        <translation>Należy zapisać plik przed jego współdzieleniem.</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="788"/>
        <source>A new update is available!</source>
        <translation>Nowa aktualizacja jest dostępna!</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="789"/>
        <source>The online app version is %1. Do you want to install this update?</source>
        <translation>Wersja dostępna online jest w wersji %1. Czy chcesz zainstalować aktualizację?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="807"/>
        <source>The maintenance tool could not be started automatically. To update the program you have to start the maintenance tool manually.</source>
        <translation>Narzędzie serwisowe nie zdołało uruchomić się automatycznie. W celu aktualizacji programu należy uruchomić narzędzie serwisowe ręcznie.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="71"/>
        <source>The document has been modified.</source>
        <translation>Dokument został zmodyfikowany.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="72"/>
        <source>Do you want to save your changes?</source>
        <translation>Czy chcesz zachować zmiany?</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="187"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="95"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../Qt/mainwindow.cpp" line="186"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="119"/>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="137"/>
        <source>Share tuning data</source>
        <translation>Współdziel dane strojenia</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="154"/>
        <source>New piano</source>
        <translation>Nowe pianino</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="157"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="159"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="167"/>
        <source>File could not be opened.</source>
        <translation>Plik nie mógł być otworzony.</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="171"/>
        <source>File could not be saved.</source>
        <translation>Plik nie mógł być zapisany.</translation>
    </message>
    <message>
        <source>Entopy piano tuner</source>
        <translation type="obsolete">Entopy piano tuner</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="191"/>
        <source>Comma-separated values</source>
        <translation>Wartości oddzielonych przecinkami</translation>
    </message>
    <message>
        <location filename="../Qt/projectmanagerforqt.cpp" line="194"/>
        <source>All files</source>
        <translation>Wszystkie pliki</translation>
    </message>
</context>
<context>
    <name>PlotsDialog</name>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="106"/>
        <source>Reset view</source>
        <translation>Zresetować widok</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="107"/>
        <source>Go first</source>
        <translation>Idź do pierwszego</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="108"/>
        <source>Go previous</source>
        <translation>Przejdź do poprzedniej</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="109"/>
        <source>Go next</source>
        <translation>Przejdź do następnego</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="110"/>
        <source>Go last</source>
        <translation>Idź do ostatniego</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="116"/>
        <source>Inh.</source>
        <translation>Inh.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="117"/>
        <source>Rec.</source>
        <translation>Rec.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="118"/>
        <source>Comp.</source>
        <translation>Comp.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="119"/>
        <source>Tun.</source>
        <translation>Tun.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="124"/>
        <source>Tuned</source>
        <translation>Tuned</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="121"/>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="283"/>
        <source>Inharmonicity</source>
        <translation>Inharmonicity</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="122"/>
        <source>Recorded</source>
        <translation>Rejestrowane</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="123"/>
        <source>Computed</source>
        <translation>Obliczane</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="137"/>
        <source>Key index</source>
        <translation>Indeks klucza.</translation>
    </message>
    <message>
        <location filename="../Qt/plotsdialog/plotsdialog.cpp" line="287"/>
        <source>Frequency deviation [cent]</source>
        <translation>Odchylenie częstotliwości [cent]</translation>
    </message>
</context>
<context>
    <name>ProgressDisplay</name>
    <message>
        <location filename="../Qt/progressdisplay.cpp" line="56"/>
        <source>Synthesizer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="982"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="985"/>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="988"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="993"/>
        <source>Images</source>
        <translation>Zdjęcia</translation>
    </message>
    <message>
        <location filename="../thirdparty/qwt/qwt_plot_renderer.cpp" line="1008"/>
        <source>Export File Name</source>
        <translation>Eksport: Nazwa pliku</translation>
    </message>
</context>
<context>
    <name>RecordingQualityBar</name>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="40"/>
        <source>Quality</source>
        <translation>Jakość</translation>
    </message>
    <message>
        <location filename="../Qt/recordingqualitybar.cpp" line="41"/>
        <source>This bar displays the quality of the recording. All of the recorded keys should have an almost equal quality before starting the calculation.</source>
        <translation>Ten pasek wyświetla jakość nagrania. Wszystkie nagrane klawisze powinny mieć w przybliżeniu podobną jakość przed rozpoczęciem obliczeń.</translation>
    </message>
</context>
<context>
    <name>RecordingStatusGraphicsView</name>
    <message>
        <location filename="../Qt/recordingstatusgraphicsview.cpp" line="33"/>
        <source>This item displays the status of the recorder. A red circle indicates that the audio signal is currently recorded. A blue rotating circle is shown when the program processes the recorded signal. A green pause symbol is displayed if you can record the next key.</source>
        <translation>Ten moduł wyświetla status nagrywania. Czerwone kółko oznacza, że sygnał dźwiękowy jest obecnie nagrywany. Niebieskie obracające się kółko jest wyświetlane gdy program przetwarza nagrany sygnał. Zielony sygnał pauzy jest wyświetlany gdy możliwe jest nagranie kolejnego klawisza.</translation>
    </message>
</context>
<context>
    <name>SignalAnalyzerGroupBox</name>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="31"/>
        <source>Signal analyzer</source>
        <translation>Analizator sygnału</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="68"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="78"/>
        <source>This label displays the current selected key.</source>
        <translation>Ta etykieta wyświetla obecnie wybrany klawisz.</translation>
    </message>
    <message>
        <location filename="../Qt/signalanalyzergroupbox.cpp" line="79"/>
        <source>This label shows the ground frequency of the selected key.</source>
        <translation>Ta etykieta wyświetla częstotliwość podstawową obecnie wybranego klawisz.</translation>
    </message>
</context>
<context>
    <name>SimpleFileDialog</name>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="92"/>
        <source>Name:</source>
        <translation>Nazwa:</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="148"/>
        <source>File existing</source>
        <translation>Plik już istnieje</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="149"/>
        <source>A file with the given name already exists at %1. Do you want to overwrite it?</source>
        <translation>Plik o podanej nazwie już istnieje w %1. Czy chcesz go zastąpić?</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Invalid filename</source>
        <translation>Nieprawidłowa nazwa pliku</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="157"/>
        <source>Please provide a valid filename.</source>
        <translation>Proszę podać poprawną nazwę pliku.</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="178"/>
        <source>Remove file</source>
        <translation>Usuń plik</translation>
    </message>
    <message>
        <location filename="../Qt/simplefiledialog.cpp" line="179"/>
        <source>Are you sure that you really want to delete the file &quot;%1&quot;?</source>
        <translation>Czy na pewno chcesz skasować plik &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>TunerApplication</name>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>Crash handler</source>
        <translation>Obsługa błędów</translation>
    </message>
    <message>
        <location filename="../Qt/tunerapplication.cpp" line="73"/>
        <source>The application exited unexpectedly on the last run. Do you want to view the last log?</source>
        <translation>Aplikacja została nieoczekiwanie zamknięta podczas ostatniego działania. Czy chcesz wyświetlić ostatni log (dziennik zdarzeń)?</translation>
    </message>
</context>
<context>
    <name>TuningGroupBox</name>
    <message>
        <source>Tuning</source>
        <translation type="vanished">Strojenie</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorGroupBox</name>
    <message>
        <location filename="../Qt/tuningindicatorgroupbox.cpp" line="27"/>
        <source>Tuning</source>
        <translation type="unfinished">Strojenie</translation>
    </message>
</context>
<context>
    <name>TuningIndicatorView</name>
    <message>
        <location filename="../Qt/tuningindicatorview.cpp" line="46"/>
        <source>This is the tuning indicator. Touch this field to toggle between spectral and stroboscopic mode. In the spectral mode you should bring the peak to the center of the window for optimal tuning. When tuning several strings of unisons at once, several peaks might appear and all of them should be tuned to match the center. In the stroboscopic mode several stripes of drifiting rainbows are shown. The stripes represent the partials. Optimal tuning of single strings is obtained when the rainbows come to a halt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeControlGroupBox</name>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="37"/>
        <source>Volume control</source>
        <translation>Ustawienia głośności</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="46"/>
        <source>Volume</source>
        <translation>Głośność</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="131"/>
        <source>Click this button to mute the speaker or headphone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="142"/>
        <source>This widgets provides settings and information about the input level of the input device.</source>
        <translation>Ten widżet dostarcza ustawień i informacji na temat poziomu wejściowego urządzenia wejściowego.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="143"/>
        <source>If the input level drops below this mark the recorder stops and does not process the input signal.</source>
        <translation>Jeżeli poziom wejściowy spadnie poniżej tego znacznika program przestaje przetwarzać sygnał wejściowy.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="144"/>
        <source>If the input level reaches this threshold the recorder starts analyzing the signal of the input device until the level drops below the &apos;Off&apos; mark.</source>
        <translation>Jeżeli poziom wejściowy przekroczy ten znacznik program rozpoczyna  przetwarzanie sygnału wejściowego.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="145"/>
        <source>Click this button to reset the automatic calibration of the input volume.</source>
        <translation>Naciśnij ten przycisk by zresetować automatyczną kalibrację wejściowego poziomu głośności.</translation>
    </message>
    <message>
        <location filename="../Qt/volumecontrolgroupbox.cpp" line="146"/>
        <source>Click this button to mute the input device.</source>
        <translation>Naciśnij ten przycisk by wyciszyć wejściowe urządzenie.</translation>
    </message>
</context>
<context>
    <name>VolumeControlLevel</name>
    <message>
        <location filename="../Qt/volumecontrollevel.cpp" line="32"/>
        <source>This bar displays the current input level.</source>
        <translation>Ten pasek wyświetla obecny poziom wejściowy. Użyj przycisku po prawej stronie by zwiększyć lub zmniejszyć ten poziom w celu dopasowania progów poziomu wejściowego pokazanych poniżej.</translation>
    </message>
</context>
<context>
    <name>ZoomedSpectrumGraphicsView</name>
    <message>
        <source>This is the tuning device. You should bring the peak and the indicator bar in the middle of the window for an optimal tuning. When tuning several strings at once, there might appear several peaks. All of them should be tuned to match the center.</source>
        <translation type="vanished">To jest urządzenie strojące. Dla optymalnego strojenia wierzchołek oraz pasek wskaźnika powinny znaleźć się na środku okna. Gdy strojonych jest kilka strun na raz, może pojawić się kilka wierzchołków. Wszystkie powinny zostać nastrojone tak, by znaleźć się w środku.</translation>
    </message>
</context>
<context>
    <name>options::OptionsDialog</name>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="48"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="70"/>
        <source>Environment</source>
        <translation>Środowisko</translation>
    </message>
    <message>
        <location filename="../Qt/options/optionsdialog.cpp" line="71"/>
        <source>Audio</source>
        <translation>Dźwięk</translation>
    </message>
</context>
<context>
    <name>options::PageAudio</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="31"/>
        <source>Input device</source>
        <translation>Urządzenie wejściowe</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="32"/>
        <source>Output device</source>
        <translation>Urządzenie wyjściowe</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudio.cpp" line="33"/>
        <source>Midi</source>
        <translation>MIDI</translation>
    </message>
</context>
<context>
    <name>options::PageAudioInputOutput</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="64"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="70"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="95"/>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="111"/>
        <source>Default</source>
        <translation>Domyślne</translation>
    </message>
    <message>
        <source>Input device</source>
        <translation type="vanished">Urządzenie wejściowe</translation>
    </message>
    <message>
        <source>Sample rates</source>
        <translation type="vanished">Częstość próbkowania</translation>
    </message>
    <message>
        <source>Sampling rates</source>
        <translation type="vanished">Częstotliwości próbkowania</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="65"/>
        <source>Device</source>
        <translation>Urządzenie</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="71"/>
        <source>Sampling rate </source>
        <translation>Częstotliwość próbkowania</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="77"/>
        <source>Open system settings</source>
        <translation>Otwórz ustawienia systemowe</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="90"/>
        <source>Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="102"/>
        <source>Buffer size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../Qt/options/audio/optionspageaudioinputoutputpage.cpp" line="243"/>
        <source>You need at least a sampling rate of %1 to record and play all keys.</source>
        <translation>Częstość próbkowania nie może być niższa od %1 by nagrać wszystkie klawisze.</translation>
    </message>
</context>
<context>
    <name>options::PageAudioMidi</name>
    <message>
        <location filename="../Qt/options/audio/optionspageaudiomidipage.cpp" line="33"/>
        <source>Midi device</source>
        <translation>Urządzenie MIDI</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironment</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="29"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironment.cpp" line="30"/>
        <source>Tuning</source>
        <translation>Strojenie</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentGeneral</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="47"/>
        <source>User Interface</source>
        <translation>Interfejs użytkownika</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="54"/>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="56"/>
        <source>&lt;System Language&gt;</source>
        <translation>&lt;Język systemowy&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="83"/>
        <source>Reactivate warnings</source>
        <translation>Reaktywować ostrzeżeniami</translation>
    </message>
    <message>
        <source>Reset warnings</source>
        <translation type="vanished">Przywróć ostrzeżenia</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="107"/>
        <source>Information</source>
        <translation>Informacja</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmentgeneralpage.cpp" line="108"/>
        <source>The language change will take effect after a restart of the entropy piano tuner.</source>
        <translation>Zmiana języka zostanie zastosowana przy ponownym uruchomieniu programu.</translation>
    </message>
</context>
<context>
    <name>options::PageEnvironmentTuning</name>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="35"/>
        <source>Disabled</source>
        <translation>Nieaktywny</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="36"/>
        <source>Synthesized key sound</source>
        <translation>Zrekonstruowany dźwięk klawisza</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="37"/>
        <source>Reference key</source>
        <translation>Klawisz odniesienia</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="38"/>
        <source>Synthesizer mode</source>
        <translation>Tryb syntetyzatora</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="42"/>
        <source>Dynamic synthesizer volume</source>
        <translation>Dynamiczna głośność</translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="48"/>
        <source>Stroboscopic tuning indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/options/environment/optionspageenvironmenttuningpage.cpp" line="54"/>
        <source>Disable automatic key selection</source>
        <translation>Wyłączanie automatycznego wyboru klucza</translation>
    </message>
</context>
</TS>
